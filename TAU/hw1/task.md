Вариант 23:
  * код: 5121
  * базис: и-не, и
  * критерий: n ЛЭ -> min(n)

1. [x] Представить развернутый алгоритм в заданном коде (таблица)
2. [x] Одноразрядный двоичный сумматор в заданном базисе
3. [x] Одноразрядный десятичный сумматор в заданном базисе
4.
  * [x] Схема преобразований в обратный код и обратно
  * [x] Схема переполнения
5. [x] Представить структурную схему 3х разрядного десятичного сумматора и описать его работу
