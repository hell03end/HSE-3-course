module Task1 (
    input wire a, b, c, d,
    output wire out
);
    assign out = ~(~(c & ~(~b & a)) | ~(b | ~a | ~c) | ~(d | a));

endmodule
