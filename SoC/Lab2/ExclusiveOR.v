module ExclusiveOR (
    input wire x1, x2,
    output wire f
);
    assign f = (x1 & ~x2) | (~x1 & x2);

endmodule
