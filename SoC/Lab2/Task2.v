module Task2 (
    input wire z, y,
    output wire out
);
    assign out = z & y;

endmodule
