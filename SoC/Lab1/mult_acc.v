`timescale 1 ns/ 10 ps

module mult_acc  #(
    parameter set = 0;
    parameter hld = 0;
)(
    output [15:0] out,
    input [7:0] ina, inb,
    input clk, clr
);
    wire [15:0] mult_out, adder_out;
    reg [15:0] out;

    function [15:0] mult;
        input [7:0] a, b;
        integer i;
        begin
            mult = 0;
            for (i = 0; i <= 7; i = i + 1) begin
                if (a[i] == 1) begin
                    mult = mult + (b << i);
                end
            end
        end
    endfunction

    assign adder_out = mult_out + out;

    always @(posedge clk or posedge clr) begin
        if (clr) begin
            out <= 16'h0000;
        end else begin
            out <= adder_out;
        end
    end

    // Function Invocation
    assign mult_out = mult(ina, inb);

    specify
        $setup (ina, posedge clk, set);
        $setup (inb, posedge clk, set);
        $hold (posedge clk, inb, hld);
        $hold (posedge clk, ina, hld);
    endspecify

endmodule
