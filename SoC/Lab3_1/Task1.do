force A0 2#0 0ns, 2#1 {50ns} -repeat 100ns;
force A1 2#0 0ns, 2#1 {100ns} -repeat 200ns;
force A2 2#0 0ns, 2#1 {200ns} -repeat 400ns;
force D 2#0 0ns, 2#1 {400ns} -repeat 800ns;
force EN 2#0 0ns, 2#1 {800ns} -repeat 1600ns;
