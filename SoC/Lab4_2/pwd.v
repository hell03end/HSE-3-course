`timescale 1 ns/ 10 ps

module pwm (
    input wire [3:0] duty,
    input wire clk,
    output wire out
);
    reg[3:0] counter;
    reg out_reg;

    initial begin
        counter = 4'b0000;
    end

    always @(posedge clk) begin
        if (counter == 4'b0) begin
            counter = counter + 4'b1;
        end
        if (counter <= duty) begin
            out_reg = 1;
        end else begin
            out_reg = 0;
        end
        counter = counter + 4'b1;
    end

    assign out = out_reg;

endmodule
