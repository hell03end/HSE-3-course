`timescale 1 ns/ 10 ps

module rstrig #(
    parameter period = 50
)(
    input wire Clk, R, S,
    output wire Q
);
    wire R_g, S_g, Qa, Qb;
    reg Clk_r, R_r, S_r;

    initial begin
        Clk_r = 0;
        forever #(period/2) Clk_r = ~Clk_r;
    end

    initial begin
        R_r = 0;
        #40 R_r = 1;
        #80 R_r = 0;
        #130 R_r = 1;
    end

    initial begin
        S_r = 0;
        #80 S_r = 1;
    end

    initial begin
        #180 $finish;
    end

    and(R_g, R, Clk);
    and(S_g, S, Clk);
    nor(Qa, R_g, Qb);
    nor(Qb, S_g, Qa);

    assign Q = Qa;
    assign Clk = Clk_r;
    assign R = R_r;
    assign S = S_r;

endmodule
