`timescale 1 ns/ 10 ps

module ram (
    input wire clk, we,
    input wire [7:0] data,
    input wire [3:0] addr,
    output wire [7:0] q
);
    reg [7:0] ram[15:0];
    reg [3:0] addr_reg;

    always @(clk) begin
        if (we) begin
            ram[addr] = data;
        end
        addr_reg = addr;
    end

    assign q = ram[addr_reg];

endmodule
