`timescale 1ns / 1ps

module bitsum (
    input wire A, B, Cin,
    output wire S, Cout
);
    wire A, B, S, Res;
    wire c1, c2, Cin, Cout;

    xor(Res, A, B);
    and(c1, A, B);
    xor(S, Cin, Res);
    and(c2, Cin, Res);
    or(Cout, c1, c2);

endmodule
