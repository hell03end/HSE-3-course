module ref_sum (
    input wire Ain, Bin, Ci,
    output wire Sout, Co
);
    wire [3:0] Sout, Ain, Bin;
    reg [4:0] S;

    always @(Ain, Bin, Ci) begin
        S = Ain + Bin + Ci;
    end

    assign Sout = S[3:0];
    assign Co = S[4];

endmodule
