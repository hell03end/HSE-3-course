`timescale 1ns / 1ps

/*
 * Top Level Testbench
 */
module test_sum;
    wire Ci, cm, cr;
    wire [3:0] Ain, Bin;
    reg [3:0] Ain_r, Bin_r;
    reg Ci_r;
    wire [3:0] res_my, res_ref;

    my_sum my_block (Ain, Bin, Ci,res_my, cm);
    ref_sum ref_block (Ain, Bin, Ci, res_ref, cr);

    initial begin
       $display("\t\t\t\tTime\tAin\tBin\tCi\tres_my\tcm\tres_ref\tcr");
       $monitor($time,,,,,Ain,,,,,Bin,,,,,Ci,,,,,res_my,,,,,,,,cm,,,,,,,res_ref,,,,,,,cr);
       #400 $finish;
    end

    initial begin
        Ain_r = 1;
        #50 Ain_r = 5;
        #50 Ain_r = 1;
        #50 Ain_r = 5;
        #50 Ain_r = 1;
        #50 Ain_r = 5;
        #50 Ain_r = 1;
        #50 Ain_r = 5;
    end

    initial begin
        Bin_r = 2;
        #100 Bin_r = 10;
        #100 Bin_r = 2;
        #100 Bin_r = 10;
    end

    initial begin
        Ci_r = 0;
        #200 Ci_r = 1;
    end

    assign Ain = Ain_r;
    assign Bin = Bin_r;
    assign Ci = Ci_r;

endmodule
