module mux_math (
    input wire [1:0] mux_control,
    input wire clk,
    input wire [15:0] add_AB, add_iAB, sub_AB, sub_iAB, mul_AB, mul_iAB, div_AB, div_iAB,
    output reg [15:0] AB_out, iAB_out
);
    always @(posedge clk) begin
        case(mux_control)
            2'b00: begin
                AB_out = add_AB;
                iAB_out = add_iAB;
            end
            2'b01: begin
                AB_out = sub_AB;
                iAB_out = sub_iAB;
            end
            2'b10: begin
                AB_out = mul_AB;
                iAB_out = mul_iAB;
            end
            2'b11: begin
                AB_out = div_AB;
                iAB_out = div_iAB;
            end
        endcase
    end

endmodule
