module state_mach (
    input wire clk, reset, Input1, Input2,
    output reg output1
);
    reg [1:0] state;
    parameter [1:0] state_A = 2'b00;
    parameter [1:0] state_B = 2'b01;
    parameter [1:0] state_C = 2'b10;

    always @(posedge clk or posedge reset) begin
        if (reset) begin
            state = state_A;
        end else begin
            case(state)
                state_A: begin
                    if (Input1 == 0) begin
                        state = state_B;
                    end else begin
                        state = state_C;
                    end
                end

                state_B: state = state_C;

                state_C: begin
                    if (Input2) begin
                        state = state_A;
                    end
                end

                default: state = state_A;
            endcase
        end
    end

    always @(state) begin
        case (state)
            state_A: output1 = 0;
            state_B: output1 = 1;
            state_C: output1 = 0;
            default: output1 = 0;
        endcase
    end

endmodule
