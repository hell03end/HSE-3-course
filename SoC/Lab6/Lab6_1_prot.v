module Lab6_1_prot(
    input wire clk, clr, input1,
    output wire output1,
    output wire [3:0] state
);
    reg clk1;
    wire clk_lab;
    reg [24:0] counter;

    initial begin
        counter = 25'b0;
    end

    Lab6_1_1 Lab6(clk_lab, clr, input1, output1, state);

    always @(posedge clk) begin
        if (counter == 25'd25000000) begin
            clk1 <= 1;
            counter <= 25'd0;
        end else begin
            clk1 <= 0;
        end
        counter <= counter + 1;
    end

    assign clk_lab = clk1;

endmodule
