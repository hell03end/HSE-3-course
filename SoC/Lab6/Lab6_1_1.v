module Lab6_1_1 (
    input wire clk, clr, input1,
    output reg output1,
    output reg [3:0] state
);
    parameter [3:0] state_A = 3'b0000;
    parameter [3:0] state_B = 3'b0001;
    parameter [3:0] state_C = 3'b0010;
    parameter [3:0] state_D = 3'b0011;
    parameter [3:0] state_E = 3'b0100;
    parameter [3:0] state_F = 3'b0101;
    parameter [3:0] state_G = 3'b0110;
    parameter [3:0] state_H = 3'b0111;
    parameter [3:0] state_I = 3'b1000;

    always @(posedge clk or posedge clr) begin
        if (clr) begin
            state = state_A;
        end else begin
            case(state)
                state_A: begin
                    if (input1 == 0) begin
                        state = state_B;
                    end else begin
                        state = state_F;
                    end
                end

                state_B: begin
                    if (input1 == 0) begin
                        state = state_C;
                    end else begin
                        state = state_F;
                    end
                end

                state_C: begin
                    if (input1 == 0) begin
                        state = state_D;
                    end else begin
                        state = state_F;
                    end
                end

                state_D: begin
                    if (input1 == 0) begin
                        state = state_E;
                    end else begin
                        state = state_F;
                    end
                end

                state_E: begin
                    if (input1 == 1) begin
                        state = state_F;
                    end
                end

                state_F: begin
                    if (input1 == 0) begin
                        state = state_B;
                    end else begin
                        state = state_G;
                    end
                end

                state_G: begin
                    if (input1 == 0) begin
                        state = state_B;
                    end else begin
                        state = state_H;
                    end
                end

                state_H: begin
                    if (input1 == 0) begin
                        state = state_B;
                    end else begin
                        state = state_I;
                    end
                end

                state_I: begin
                    if (input1 == 0) begin
                        state = state_B;
                    end
                end

                default: state = state_A;
            endcase
        end
    end

    always @(state) begin
        case (state)
            state_A: output1 = 0;
            state_B: output1 = 0;
            state_C: output1 = 0;
            state_D: output1 = 0;
            state_E: output1 = 1;
            state_F: output1 = 0;
            state_G: output1 = 0;
            state_H: output1 = 0;
            state_I: output1 = 1;
            default: output1 = 0;
        endcase
    end

endmodule
