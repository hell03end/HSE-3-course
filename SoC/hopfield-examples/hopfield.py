import numpy as np
import random
import Image
import os
import re

def mat2vec(matrix):
    """ convert matrix to a vector """
    m = matrix.shape[0] * matrix.shape[1]
    tmp = np.zeros(m)

    c = 0
    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            tmp[c] = matrix[i, j]
            c += 1
    return tmp


def create_W(image_vec):
    """ Create Weight matrix for a single image """
    if len(image_vec.shape) != 1:
        raise ValueError("The input is not vector")
    else:
        w = np.zeros([len(image_vec), len(image_vec)])
        for i in range(len(image_vec)):
            for j in range(i, len(image_vec)):
                if i == j:
                    w[i, j] = 0
                else:
                    w[i, j] = image_vec[i] * image_vec[j]
                    w[j, i] = w[i, j]  # symmetrical matrix
    return w


def readImg2array(file, size, threshold=145):
    """ Read Image file and convert it to Numpy array """
    pilIN = Image.open(file).convert(mode="L")
    pilIN= pilIN.resize(size)
    # pilIN.thumbnail(size,Image.ANTIALIAS)
    imgArray = np.asarray(pilIN, dtype=np.uint8)
    x = np.zeros(imgArray.shape, dtype=np.float)
    # binarise
    x[imgArray > threshold] = 1  # norm to 0-1
    x[x == 0] = -1
    return x


def array2img(data, outFile=None):
    """ Convert Numpy array to Image file like Jpeg """
    # data is 1 or -1 matrix
    y = np.zeros(data.shape, dtype=np.uint8)
    y[data == 1] = 255
    y[data == -1] = 0
    img = Image.fromarray(y, mode="L")
    if outFile is not None:
        img.save(outFile)
    return img


def update(w, y_vec, theta=0.5, n_times=100):
    """ update weights """
    for s in range(n_times):
        i = random.randint(0, len(y_vec) - 1)
        u = np.dot(w[i][:], y_vec) - theta
        if u > 0:
            y_vec[i] = 1
        elif u < 0:
            y_vec[i] = -1
    return y_vec


def hopfield(train_files,
             test_files,
             theta=0.5,
             n_times=1000,
             size=(100,100),
             threshold=60,
             current_path=None):
    """ The following is training pipeline """
    num_files = 0  #number of input files
    for path in train_files:
        x = readImg2array(file=path, size=size, threshold=threshold)
        x_vec = mat2vec(x)
        if num_files == 0:
            w = create_W(x_vec)
        else:
            tmp_w = create_W(x_vec)
            w = w + tmp_w
        num_files += 1

    #Import test data
    counter = 0
    for path in test_files:
        y = readImg2array(file=path, size=size, threshold=threshold)
        oshape = y.shape
        y_vec = mat2vec(y)
        y_vec_after = update(w=w, y_vec=y_vec, theta=theta, n_times=n_times)
        y_vec_after = y_vec_after.reshape(oshape)
        outfile = current_path + "/after_" + str(counter) + ".jpeg"
        array2img(y_vec_after, outFile=outfile)
        counter += 1


if __name__ == '__main__':
    #First, you can create a list of input file path
    current_path = os.getcwd()
    train_paths = []
    path = current_path + "/train_pics/"
    for i in os.listdir(path):
        if re.match(r'[0-9a-zA-Z-_]+\.jpe?g', i):
            train_paths.append(path + i)

    #Second, you can create a list of sungallses file path
    test_paths = []
    path = current_path + "/test_pics/"
    for i in os.listdir(path):
        if re.match(r'[0-9a-zA-Z-_]+\.jpe?g', i):
            test_paths.append(path + i)

    #Hopfield network starts!
    hopfield(train_files=train_paths, test_files=test_paths, theta=0.5,
             n_times=20000, size=(100, 100), threshold=60,
             current_path=current_path)
