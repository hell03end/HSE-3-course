//-----------------------------------------------------
// Имя модуля : up_counter
// Имя файла : up_counter.v
// Функц. Назначение : инкрементирующий счётчик
// Программист : www.portal-ed.ru
//-----------------------------------------------------
module up_counter (
out , // Выходная шина счётчика
clk , // Тактовый вход
reset // Вход сброса
);
//----------Выходные порты--------------
output [7:0] out;
//------------Входные порты--------------
input clk, reset;
//------------Внутренние переменные--------
reg [7:0] out;
//-------------Начало кода-------
always @(posedge clk or posedge reset)
	if (reset) 
	begin
		out <= 8'b0 ;
   end 
	else 
	begin
		out <= out + 1;
	end
endmodule //Конец модуля up_counter