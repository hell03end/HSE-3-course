`timescale 1 ns / 1 ps

/*
 * Synchronizes switch and button inputs with a slow sampled shift register
 */
module ButtonDebouncer  #(
    parameter SHIFT_RANK = 10 // rank (length) of shift register
)(
    input wire clk,
    input wire in,
    output reg out
);

    reg [1:0] swR;
    reg [SHIFT_RANK:0] swCount;
    wire swChangeF = (out != swR[1]);
    wire swCountMax = &swCount;

    always @(posedge clk) begin
        swR <= {swR[0], in};
        if (swChangeF) begin
            swCount <= swCount + 1'b1;
            if (swCountMax) begin
                out <= ~out;
            end
        end
        else begin
            swCount <= 0;
        end
    end

endmodule
