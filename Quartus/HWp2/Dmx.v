`timescale 1 ns / 1 ps

/*
 * TODO: add description.
 */
module Dmx  #(
    parameter WIDTH = 2
)(
    input wire [WIDTH-1:0] in,
    output reg [WIDTH*WIDTH-1:0] out
);
    always @(in) begin
        out <= 0 | 1 << in;
    end

endmodule
