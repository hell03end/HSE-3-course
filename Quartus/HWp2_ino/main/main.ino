#include <stdarg.h>

#define PC_FREQ 9600    // for PC communication by Serial
#define FPGA_FREQ 9600  // for communication with fpga by SoftwareSerial

#define IS_DIGIT(x) ((x) > 47 && (x) < 58)
#define TO_DIGIT(x) ((x)-48)
#define TO_ALPHA(x) ((x) + 48)

void prints(int n_args, ...) {
  va_list args;  // optional args
  va_start(args, n_args);
  for (int i = 0; i < n_args; ++i) {
    Serial.print(va_arg(args, char*));
  }
  Serial.println();
  va_end(args);
}

void setup() {
  Serial.begin(PC_FREQ);
  while (!Serial) {
  }
  Serial.println("init");

  Serial1.begin(FPGA_FREQ);
  while (!Serial1) {
  }
}

void loop() {
  // on receive input from PC
  if (Serial.available()) {
    char alpha = (char)Serial.read();
    char alpha_str[2] = {alpha};
    if (!IS_DIGIT(alpha)) {
      prints(3, "`", alpha_str, "`\tis not a digit!");
    } else {
      char digit = TO_DIGIT(alpha);
      prints(2, "Send: ", alpha_str);
      Serial.println(digit, BIN);
      Serial1.write((char)digit);
    }
  }
  // on receive input from fpga
  while (Serial1.available() > 0) {
    char data_byte = (char)Serial1.read();
    char alpha = TO_ALPHA(data_byte);
    char alpha_str[2] = {alpha};
    prints(2, "[UART] fpga:\t", alpha_str);
  }
}
