Science and Research Seminar
============================
At HSE University, Moscow.

rsc
---

### Content:
**This is supporting information.**
* `IDEF0-template.xml` - [Draw.io](https://www.draw.io/) template for [IDEF0](https://en.wikipedia.org/wiki/IDEF0) diagrams.
* `materialsUML.pdf` - copy of course guidelines.
* `gost.md` - copy of comments for course guidelines.
* `TR.md` - some notes about Technical Requirements by State Standard (*GOST*).
