АИС для обслуживания клиентов сервиса, предоставляющего датчики для автоматического анализа технологических процессов производства.
===============================================

Задача
------
Информационная поддержка деятельности малого предприятия по предоставлению платформ, собирающих данные о процессе производства.


Описание предметной области
---------------------------
Компания предоставляет клиентам услуги по использованию технологической платформы для контроля за технологическим процессом на производстве. Для получения услуг, клиент регистрируется в системе. Далее, клиенту устанавливается аппаратный комплекс (система) содержащий датчики и систему по сбору данных и отправки их на сервера компании. Датчики записывают данные, система собирает их и отправляет на сервер с определенной периодичностью. Серверная часть системы выполняет анализ полученных данных (ищет в них ошибки, строит прогнозы). Полученные выводы соответствуют конкретным записям о полученных данных (например, «запись 123 – не соответствует норме, возможна авария»). При выходе из строя датчики заменяются. При отказе от предоставляемых услуг, компания изымает систему (возможно переиспользование).


### Функции, осуществляемые АИС
<!-- Если нужно, измени, я не трогал -->
1. Учет поступающих с датчиков данных;
2. Ведение списка датчиков;
3. Ведение списка платформ;
4. Ведение списка клиентов;
5. Ведение списка дополнительных услуг;
6. Ведение списка выводов по полученным данным;
7. Поиск некорректных данных;
8. Учет стоимости услуг;
9. Учет платежей;
10. Предоставление скидок для постоянных клиентов: величина скидки зависит от количества платформ и датчиков на одного клиента.


### Готовые запросы
<!-- Если нужно, измени, я не трогал -->
1. Список типов датчиков;
2. Список введенных в эксплуатацию платформ;
3. Список данных с ошибками;
4. Расчет тарифного плана клиента;
5. Список клиентов, с числом датчиков > 30 на одну платформу;
6. Список платформ с наибольшей нагрузкой за последний месяц;
7. Список должников;
8. Список постоянных клиентов;
9. Список датчиков с истекшим сроком эксплуатации;
10. Список датчиков, чей срок эксплуатации стремится к завершению (заданный порог).


Инфологический анализ
---------------------

### Отношения
<!-- Делал не я, поэтому не трогал - если нужно что-то поправить, поправь -->
1. У каждого клиента может быть несколько платформ, у каждой платформы может быть один владелец (клиент);
2. У каждой платформы может быть несколько датчиков, у каждого датчика может быть одна платформа;
3. С каждого датчика может приходить много данных, и у каждой записи данных может быть один датчик;
4. Записи данных содержат информацию о платформе, к которой подключены датчики (для обеспечения безопасной замены датчиков без потери данных);
5. У каждого клиента есть много платежей, каждый платеж совершается 1 клиентом;
6. У каждого клиента может быть много дополнительных услуг, каждой услугой может пользоваться много клиентов;
7. Каждый набор данных может иметь свой анализ, каждый анализ относится к 1 набору данных.


Сущности (реляционные отношения)
--------------------------------
<!-- Изменил то, что было ранее -->

* `C(k)` - строка фиксированной длины из k символов;
* `D` - дата;
* `N(k, p)/N(k)` - число максимальной длины k, с p знаками после запятой (0 - целое);
* `V(k)` - строка переменной длины;
* `T` - текст = V(512).

### Клиенты [`Clients`]
Потенциальный первичный ключ - паспорт/телефон/email, однако, данные поля слишком длинные и могут измениться, поэтому вводится суррогатный первичный ключ.

|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|id|`id`|N(4)|**суррогатный первичный ключ**|
|Паспортные данные|`cl_passport`|V(64)|*уникальное*, *обязательное*|
|Имя|`cl_name`|V(25)|только буквы, *обязательное*|
|Фамилия|`cl_surname`|V(25)|только буквы, *обязательное*|
|Отчество|`cl_lastname`|V(25)|только буквы|
|Телефон|`cl_phone`|C(10)|только цифры, *уникальное*, *обязательное*, *многозначное*|
|Email|`cl_email`|V(128)|*уникальное*, *обязательное*|
|Адрес|`cl_address`|V(256)||

### Платформы [`Systems`]
Потенциальный первичный ключ - api ключ, однако это поле слишком длинное и может измениться, поэтому вводится суррогатный первичный ключ.

|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|id|`id`|N(8)|**суррогатный первичный ключ**|
|API ключ|`sys_api_key`|C(128)|*уникальное*, *обязательное*|
|Модель|`sys_model`|V(128)|*обязательное*, *принадлежит набору значений*|
|Адрес расположения|`sys_address`|V(256)|*обязательное*|
|Начало эксплуатации|`sys_start_date`|D|`<= Now()`, *обязательное*|
|Срок эксплуатации|`sys_lifecycle`|N(10)|`> 0`, *обязательное*|
|Время эксплуатации|`sys_lifetime`|N(10)|`>= 0`, автоматически = `start_date - Now()`|

### Датчики [`Sensors`]
Потенциальные первичные ключи отсутствуют, поэтому вводится суррогатный.

|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|id|`id`|N(16)|**суррогатный первичный ключ**|
|Идентификатор платформы|`sns_system_id`|N(8)|**внешний ключ** (к `Systems`), *обязательное*|
|Модель|`sns_model`|V(128)|*обязательное*, *принадлежит набору значений*|
|Единица измерения|`sns_unit`|V(32)|*обязательное*|
|Описание|`sns_description`|T||
|Погрешность измерения|`sns_error`|N(4)|*обязательное*|
|Начало эксплуатации|`sns_start_date`|D|`<= Now()`, *обязательное*|
|Срок эксплуатации|`sns_lifecycle`|N(10)|`> 0`, *обязательное*|
|Время эксплуатации|`sns_lifetime`|N(10)|`>= 0`, автоматически = `start_date - Now()`|

### Данные [`Data`]
Потенциальные первичные ключи отсутствуют, поэтому вводится суррогатный.

|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|id|`id`|N(32)|**суррогатный первичный ключ**|
|Идентификатор платформы|`dt_system_id`|N(8)|**внешний ключ** (к `Systems`), *обязательное*|
|Идентификатор датчика|`dt_sensor_id`|N(16)|**внешний ключ** (к `Sensors`), *обязательное*|
|Время получения|`dt_timestamp`|D|*обязательное*, автоматически `= Now()`|
|Показатель|`dt_data`|N(16,4)|*обязательное*|

### Платежи [`Payments`]
Потенциальные первичные ключи отсутствуют (комбинация полей `client_id` и `service_id` не подходит, поскольку один клиент может оплачивать множество услуг множество раз), поэтому вводится суррогатный.

|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|id|`id`|N(16)|**суррогатный первичный ключ**|
|Идентификатор клиента|`pay_client_id`|N(4)|**внешний ключ** (к `Clients`), *обязательное*|
|Идентификатор услуги|`pay_service_id`|N(4)|**внешний ключ** (к `Services`), *обязательное*|
|Сумма|`pay_amount`|N(8,4)|`> 0`, *обязательное*|
|Время получения|`pay_timestamp`|D|`<= Now()`, *обязательное*|
|Описание|`pay_description`|T||
|Статус выполнения|`pay_status`|V(64)|*обязательное*, *принадлежит набору значений*|

### Услуги [`Services`]
Потенциальный первичный ключ - название, однако это поле слишком длинное и может измениться, поэтому вводится суррогатный первичный ключ.

|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|id|`id`|N(4)|**суррогатный первичный ключ**|
|Название|`srv_title`|V(128)|*уникальное*, *обязательное*|
|Описание|`srv_description`|T|*обязательное*|
|Цена|`srv_price`|N(8,4)|`>= 0`, *обязательное*|

### Анализ [`Analysis`]
Потенциальные первичные ключи отсутствуют, поэтому вводится суррогатный.

|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|id|`id`|N(32)|**суррогатный первичный ключ**|
|Идентификатор данных|`an_data_id`|N(32)|**внешний ключ** (к `Data`), *обязательное*|
|Заключение|`an_status`|V(128)|*принадлежит набору значений*, *обязательное*|
|Описание|`an_description`|T||
|Время создания|`an_timestamp`|D|`= Now()`, `2017 < an_timestamp < Now()`, *обязательное*|

### Вспомогательные отношения
Не требуют внешнего ключа, поскольку на них никто не ссылается, однако для обеспечения уникальности записей введен составной ключ из комбинации внешних.

#### Подписка [`Subscription`]
Требуется для связи клиентов и услуг, которые они должны оплатить. Отношение Платежи не подходит, поскольку требуемый кортеж создается не в момент выбора клиентом услуги, а при совершении первого платежа.

|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|Идентификатор клиента|`sb_client_id`|N(4)|**внешний ключ** (к `Clients`), **составной первичный ключ**|
|Идентификатор услуги|`sb_service_id`|N(4)|**внешний ключ** (к `Services`), **составной первичный ключ**|
|Дата начала|`sb_start_date`|D|автоматически `= Now()`, *обязательное*|

#### Аренда [`Rent`]
|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|Идентификатор клиента|`rnt_client_id`|N(4)|**внешний ключ** (к `Clients`), **составной первичный ключ**|
|Идентификатор платформы|`rnt_system_id`|N(8)|**внешний ключ** (к `Systems`), **составной первичный ключ**|
|Дата начала|`rnt_start_date`|D|автоматически `= Now()`, *обязательное*|


Связи
-----
<!-- TODO: исправить связи (их стало больше) -->
* Клиент - (владение) - Платформа
* Датчик - (подключение) - Платформа


Пользователи
------------
<!-- Если нужно что-то править, поправь -->
1. Клиенты:
    * просмотр агрегированных данных о показателях датчиков;
    * получение информации о состоянии «счета» (список не оплаченных услуг, проведенных платежей);
    * изменение наименований датчиков;
    * подключение/отключение услуг;
    * изменение личной информации;
    * просмотр результатов анализа данных.
2. Поставщики услуг:
    * управление системами и услугами;
    * конфигурация систем и датчиков;
    * просмотр списков должников (по количеству неоплаченных услуг: сумма платежей < сумма стоимости услуг * период использования).
3. Системы сбора данных:
    * запись данных, полученных с датчиков;
4. Внутренняя логика приложения:
    * проведение платежей;
    * внесение пользователей в список должников (если последняя оплата производилась позже месяца назад);
    * анализ данных (поиск ошибок, трендов и т.д.);
    * обработка действий пользователей;
    * обработка действий поставщиков услуг.


Выбор аппаратной и программной платформ
---------------------------------------
Система обслуживает датчики внутри одной организации, следовательно, имеется относительно небольшой набор требований.
Объем хранилища обусловлен непрерывно поступающими данными с датчиков (0.5Кб), которые необходимо хранить.

### Требования к аппаратному обеспечению:
* 512 МБ RAM (обработка HTTP запросов, операции с бд)
* 1 CPU
* 20 ГБ хранилища (место на хранение приложения, обновлений и содержимого бд)
* 10 Гб/мес трафика
* 512 Мбит/с на канал передачи данных

### Требования к программному обеспечению:
* Unix-based OS (например, debian/debian-based системы)
* MySQL Community Server 5.7 и старше
* Python 3.6 и старше

Так как сервис представляет собой клиент-серверное веб приложение, то предпочтительным будет использование Unix-based OS.
На самом деле можно воспользоваться любой SQL СУБД, однако выбор в пользу MySQL был сделан из-за простоты эксплуатации и бесплатной доступности.


Нормализация отношений
----------------------

### 1НФ

Аттрибуты "время эксплуатации" отношений Датчики и Платформы не несут полезной информации и будут реализованы в виде представлений (готовых запросов), а из отношений удалены.

#### Разбиение сложных (составных) аттрибутов на простые.
1. Клиенты:
    * паспортные данные -> номер паспорта (уникальное), дата выдачи, кем выдан; все поля обязательные; связь 1:1 по номеру паспорта (то, что он меняется допустимо из-за степени отношения, а также поскольку сведения носят справочный характер)
    * адрес -> страна, город, улица, дом; все поля обязательные
2. Платформы
    * адрес расположения -> страна, город, улица, дом; все поля обязательные
3. Анализ
    * заключение -> тип, вывод; все поля обязательные, принадлежат набору значений

#### Разбиение многозначных аттрибутов.
Клиенты: телефон -> отношение "Номера телефонов" (первичный ключ не требуется)


### 2НФ
В нашем случае составные первичные ключи имеют вспомогательные отношения Аренда и Подписка. Неключевые атрибуты этих отношений функционально полно зависят от составных первичных ключей.

### 3НФ
В отношении Платформы аттрибут "срок эксплуатации" зависит не от первичного ключа а от аттрибута "модель", поэтому имеет смысл вынести их в отдельное отношение "Модели платформ" добавив суррогатный первичный ключ, поскольку поле "модель" представляет собой длинную символьную строку. Так как одинаковая модель может быть у многих датчиков, образованная связь будет 1:n и суррогатный первичный ключ станет внешним для отношения Платформы.

В отношении Датчики проделаем те же действия для аттрибутов "модель", "срок эксплуатации", "единица измерения" и "погрешность измерения" для создания отношения "Модели датчиков".

В отношении Услуги аттрибут "описание" зависит от поля "название", а не внешнего ключа, однако декомпозиция избыточна, поскольку поле "название" уникальное, следовательно между отношениями будет существовать связь 1:1.

В отношении Анализ проделаем те же действия для полученных аттрибутов "тип" и "вывод" для создания отношения Заключения.

### 4НФ
Отношение Заключения нарушают 4НФ, т.к. не всякий тип привязан к конкретному выводу, следовательно, имеются многозначные зависимости в одном отношении. Декомпозиция будет избыточным решением, поэтому аттрибут "тип" будет перенесен обратно в отношение Анализ с изменением типа на целочисленный (приемлемый вид для автоматической обработки), а отношение Заключения переименовано в Выводы.

Отношение Адреса также нарушает 4НФ, т.к. город зависит от страны, а улица от города. Однако, в этом случае какие-либо изменения избыточны, поскольку данные сведения носят справочный характер.


Отношения после нормализации
----------------------------
Приведены только измененные и добавленные отношения.

### Клиенты [`Clients`]
|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|id|`id`|N(4)|**суррогатный первичный ключ**|
|Идентификатор паспортных данных|`cl_passport_id`|N(10)|**внешний ключ** (к `Passports`), *уникальное*, *обязательное*|
|Идентификатор адреса|`cl_address_id`|N(8)|**внешний ключ** (к `Addresses`)|
|Имя|`cl_name`|V(25)|только буквы, *обязательное*|
|Фамилия|`cl_surname`|V(25)|только буквы, *обязательное*|
|Отчество|`cl_lastname`|V(25)|только буквы|
|Email|`cl_email`|V(128)|*уникальное*, *обязательное*|

### Номера телефонов [`Phones`]
Первичный ключ вводится для обеспечения уникальности хранимых данных (один телефон не может быть одновременно у нескольких клиентов). Также, становится возможным более эффективный поиск клиента по номеру телефона.

Поскольку номер телефона является обязательным для кортежей таблицы Клиенты, также как для таблицы "Номера телефонов", требуется сделать одну из связей необязательной. Так как регистрация телефонов происходит при регистрации нового клиента, связь оставлена обязательной со стороны отношения Клиенты.

|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|Телефон|`ph_phone`|N(10)|**первичный ключ**|
|Идентификатор клиента|`ph_client_id`|N(4)|**внешний ключ** (к `Clients`)|

### Паспортные данные [`Passports`]
Связь 1:1 с отношением Клиенты.

|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|Номер паспорта|`pas_number`|N(10)|**первичный ключ**|
|Кем выдан|`pas_department`|V(32)|*обязательное*|
|Когда выдан|`pas_received`|D|*обязательное*, `1965 < pas_received < Now()`|

### Адреса [`Addresses`]
|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|id|`id`|N(8)|**суррогатный первичный ключ**|
|Страна|`ad_country`|V(32)|*обязательное*, *принадлежит набору значений*|
|Город|`ad_city`|V(32)|*обязательное*|
|Улица|`ad_street`|V(64)|*обязательное*|
|Дом|`ad_house`|V(16)|*обязательное*|

### Платформы [`Systems`]
|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|id|`id`|N(8)|**суррогатный первичный ключ**|
|Идентификатор адреса расположения|`sys_address_id`|N(8)|**внешний ключ** (к `Addresses`), *обязательное*|
|Идентификатор модели|`sys_model_id`|N(4)|**внешний ключ** (к `SystemsModels`), *обязательное*|
|API ключ|`sys_api_key`|C(128)|*уникальное*, *обязательное*|
|Начало эксплуатации|`sys_start_date`|D|`2017 < sys_start_date <= Now()`, *обязательное*|

### Модели платформ [`SystemsModels`]
|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|id|`id`|N(4)|**суррогатный первичный ключ**|
|Название модели|`sysm_name`|V(128)|*обязательное*|
|Срок эксплуатации|`sysm_lifecycle`|N(10, 3)|`> 0`, *обязательное*|

### Датчики [`Sensors`]
|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|id|`id`|N(16)|**суррогатный первичный ключ**|
|Идентификатор платформы|`sns_system_id`|N(8)|**внешний ключ** (к `Systems`), *обязательное*|
|Идентификатор модели|`sns_model_id`|N(8)|**внешний ключ** (к `SensorsModels`), *обязательное*|
|Описание|`sns_description`|T||
|Начало эксплуатации|`sns_start_date`|D|`2017 < sns_start_date <= Now()`, *обязательное*|

### Модели датчиков [`SensorsModels`]
|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|id|`id`|N(8)|**суррогатный первичный ключ**|
|Название модели|`snsm_name`|V(128)|*обязательное*|
|Единица измерения|`snsm_unit`|V(16)|*обязательное*|
|Погрешность измерения|`snsm_error`|N(4, 3)|*обязательное*|
|Срок эксплуатации|`snsm_lifecycle`|N(10, 3)|`> 0`, *обязательное*|

### Анализ [`Analysis`]
|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|id|`id`|N(32)|**суррогатный первичный ключ**|
|Идентификатор данных|`an_data_id`|N(32)|**внешний ключ** (к `Data`), *обязательное*|
|Идентификатор типа|`an_type_id`|N(8)|**внешний ключ** (к `Conclusions`), *обязательное*|
|Описание|`an_description`|T||
|Время создания|`an_timestamp`|D|`= Now()`, `2017 < an_timestamp < Now()`, *обязательное*|

### Выводы [`Conclusions`]
|Содержание поля|Имя поля|Тип, длина|Примечания|
|---------------|--------|----------|----------|
|тип|`con_type`|N(8)|**первичный ключ**|
|вывод|`con_conclusion`|V(128)|*обязательное*|


Определение дополнительных ограничений целостности
--------------------------------------------------

1. Для аттрибута "email" отношения Клиенты должно быть применимо следующее регулярное выражение: `\b[a-zA-Z0-9\._-]{2,}@([a-zA-Z]{2,}\.)?[a-zA-Z]{2,}\.ru\b`, что, возможно, не будет реализовано в рамках данной работы (легче реализовать программно: через внешнее прикладное решение или триггеры)
2. Аттрибут "страна" в отношении Адреса может принимать одно из следующих значений: "Россия", "Украина", "Казахстан", "Беларусь"
3. Аттрибут "город" в отношении Адреса может принимать одно из значений, соответствующих городам выбранной страны (реализуется программно: через внешнее прикладное решение или триггеры)
4. Аттрибут "статус выполнения" отношения Платежи может принимать одно из следующих значений: "выполнен", "отклонен", "ожидает подтверждения"


Описание групп пользователей и прав доступа
-------------------------------------------
Введем следующие обозначения:

* `S` – чтение данных (select); 
* `I` – добавление данных (insert); 
* `U` – модификация данных (update); 
* `D` – удаление данных(delete).

В большом количестве случаев клиенты имеют доступ не ко всему отношению, а только к своим данным о нем через внутреннюю логику приложения. Такой доступ равносилен отсутствию прав на просмотр/добавление/модификацию и удаления данных из отношения, что отражено в таблице. 

Поставщики услуг, в свою очередь, в основном также взаимодействуют с системой через внутреннюю логику приложения, однако имеют непосредственный доступ к некоторым отношениям.

Логика устройства системы предполагает отсутствие возможности модификации и удаления данных из соответствующего отношения.

|                   |Клиенты|Поставщики услуг|Системы сбора данных|Внутренняя логика приложения|
|-------------------|-------|----------------|--------------------|----------------------------|
|Платежи            |       |S               |                    |SI                          |
|Клиенты            |       |S               |                    |SUID                        |
|Платформы          |       |SUID            |                    |SUID                        |
|Услуги             |S      |SUID            |                    |SUID                        |
|Датчики            |       |SUID            |                    |SUID                        |
|Данные             |       |S               |I                   |S                           |
|Выводы             |       |S               |                    |SUID                        |
|Анализ             |       |S               |                    |SID                         |
|Модели датчиков    |S      |SUID            |                    |SUID                        |
|Модели платформ    |S      |SUID            |                    |SUID                        |
|Адреса             |       |S               |                    |SUID                        |
|Паспортные данные  |       |S               |                    |SUID                        |
|Номера телефонов   |       |S               |                    |SUID                        |
|Аренда             |       |S               |                    |SUID                        |
|Подписка           |       |S               |                    |SUID                        |


Реализация проекта базы данных
==============================

Создание таблиц
---------------
Записано в отдельном `.SQL` файле.


Создание представлений
----------------------
<!-- TODO: создать представления -->
Записано в отдельном `.SQL` файле.

### Права доступа к представлениям
<!-- TODO: определить права доступа к созданным представлениям -->
|                   |Клиенты|Поставщики услуг|Системы сбора данных|Внутренняя логика приложения|
|-------------------|-------|----------------|--------------------|----------------------------|
||||||


Назначение прав доступа
-----------------------
Для простоты выделим следующих пользователей:
1. `users` - обобщение всех клиентов;
2. `suppliers` - обобщение всех поставщиков услуг (ниже не приведено назначение прав на чтение, поскольку эта группа пользователей имеет доступ на чтение ко всем отношениям БД);
3. `systems` - системы сбора данных (могут быть представленны единственным пользователем);
4. `app` - внутренняя логика приложения (единственный пользователь, ниже приведены только запросы на назначение прав доступа, отличных от SUID).
5. `root` - администратор БД (имеет полный доступ к БД).

```SQL
grant select on Services to users;
grant select on SensorsModels to users;
grant select on SystemsModels to users;

grant select, update, insert, delete on Systems to suppliers;
grant select, update, insert, delete on Services to suppliers;
grant select, update, insert, delete on Sensors to suppliers;
grant select, update, insert, delete on SystemsModels to suppliers;
grant select, update, insert, delete on SensorsModels to suppliers;
grant select on Payments to suppliers;
grant select on Clients to suppliers;
grant select on Data to suppliers;
grant select on Conclusions to suppliers;
grant select on Analysis to suppliers;
grant select on Addresses to suppliers;
grant select on Passports to suppliers;
grant select on Phones to suppliers;
grant select on Rent to suppliers;
grant select on Subscription to suppliers;

grant insert on Data to Systems;

grant select, update, insert, delete on Systems to app;
grant select, update, insert, delete on Services to app;
grant select, update, insert, delete on Sensors to app;
grant select, update, insert, delete on SystemsModels to app;
grant select, update, insert, delete on SensorsModels to app;
grant select, insert on Payments to app;
grant select, update, insert, delete on Clients to app;
grant select on Data to app;
grant select, update, insert, delete on Conclusions to app;
grant select, insert, delete on Analysis to app;
grant select, update, insert, delete on Addresses to app;
grant select, update, insert, delete on Passports to app;
grant select, update, insert, delete on Phones to app;
grant select, update, insert, delete on Rent to app;
grant select, update, insert, delete on Subscription to app;
```


Создание индексов
-----------------
<!-- TODO: создать индексы где это нужно -->


Разработка стратегии резервного копирования
-------------------------------------------
Интенсивность обновления некоторых отношений разработанной базы данных очень высока, к тому же, сервер содержащий базу данных работает в следующем режиме: 7 дней в неделю с несколькочасовыми перерывами каждые сутки (без выключения), однако некоторые таблицы базы данных обновляются не часто (клиенты, паспортные данные, номера телефонов, платформы, модели платформ, модели датчиков, адреса и т. д.). К тому же, редко обновляемые данные являются наиболее чувствительными к утрате (Клиентская база, данные клиентов и т. д.). Для обеспечения сохранности данных требуется разделить резервное копирование на: 
1. полное копирование относительно редко обновляемых данных в моменты простоя сервера (определяется администратором);
2. периодическое (несколько раз в день во время работы системы) создание копий последних изменений часто добавляемых данных.

Второй вариант возможен, поскольку данные часто изменяемых таблиц записываются в них последовательно и не изменяются/удаляются после записи (записи о транзакциях данных, платежей и т. д.).
