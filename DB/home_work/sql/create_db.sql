-- Use `source $PATH_TO_FILE/create_db.sql;` to run this file in MySQL shell

create database if not exists SensorsSystem default character set utf8 default collate utf8_general_ci;

use SensorsSystem;

create table if not exists PassportDepartments (
    pas_number char(6) not null,
    pas_department varchar(32) not null,
    pas_received datetime not null,
    constraint pk_passports primary key (pas_number)
) engine=innodb default character set=utf8 insert_method=last;

create table if not exists Addresses (
    id numeric(10, 0) not null,
    ad_name varchar(64) not null,
    ad_fk numeric(10, 0),
    constraint pk_addresses primary key (id),
    constraint fk_addresses foreign key (ad_fk)
        references Addresses(id)
) engine=innodb default character set=utf8 insert_method=last;

create table if not exists SystemsModels (
    id numeric(4, 0) not null,
    sysm_name varchar(128) not null,
    sysm_lifecycle numeric(10, 3) not null,
    constraint pk_systemsmodels primary key (id),
    constraint ck_sysm_lifecycle check(sysm_lifecycle > 0)
) engine=innodb default character set=utf8 insert_method=last;

create table if not exists SensorsModels (
    id numeric(4, 0) not null,
    snsm_name varchar(128) not null,
    snsm_unit varchar(16) not null,
    snsm_error numeric(4, 3) not null,
    snsm_lifecycle numeric(10, 3) not null,
    constraint pk_sensorsmodels primary key (id),
    constraint ck_snsm_lifecycle check(snsm_lifecycle > 0)
) engine=innodb default character set=utf8 insert_method=last;

create table if not exists Systems (
    id numeric(8, 0) not null,
    sys_address_id numeric(10, 0) not null,
    sys_model_id numeric(5, 0) not null,
    sys_client_id numeric(4, 0),
    sys_api_key char(128) not null,
    sys_start_date datetime,
    constraint pk_systems primary key (id),
    constraint fk_systems_address foreign key (sys_address_id)
        references Addresses(id),
    constraint fk_systems_systemsmodels foreign key (sys_model_id)
        references SystemsModels(id),
    constraint fk_systems_clients foreign key (sys_client_id)
        references Clients(id),
    constraint uk_api_key unique (sys_api_key)
) engine=innodb default character set=utf8 insert_method=last;

create table if not exists Sensors (
    id numeric(10, 0) not null,
    sns_system_id numeric(8, 0),
    sns_model_id numeric(4, 0) not null,
    sns_description varchar(512),
    sns_start_date datetime default now(),
    constraint pk_sensors primary key (id),
    constraint fk_sensors_systems foreign key (sns_system_id)
        references Systems(id),
    constraint fk_sensors_sensorsmodels foreign key (sns_model_id)
        references SensorsModels(id)
) engine=innodb default character set=utf8 insert_method=last;

create table if not exists Clients (
    id numeric(4, 0),
    cl_passport numeric(10, 0) not null,
    cl_passdep_id char(6) not null,
    cl_address_id numeric(10, 0),
    cl_name varchar(25) not null,
    cl_surname varchar(25) not null,
    cl_lastname varchar(25),
    cl_email varchar(128) not null,
    cl_reg_date datetime not null default now(),
    constraint pk_clients primary key (id),
    constraint fk_clients_passport foreign key (cl_passdep_id)
        references PassportDepartments(pas_number),
    constraint fk_clients_address foreign key (cl_address_id)
        references Addresses(id),
    constraint uk_email unique (cl_email),
    constraint uk_passport unique (cl_passport)
) engine=innodb default character set=utf8 insert_method=last;

create table if not exists Phones (
    ph_phone numeric(10, 0) not null,
    ph_client_id numeric(4, 0) not null,
    constraint pk_phones primary key (ph_phone),
    constraint fk_phones_clients foreign key (ph_client_id)
        references Clients(id)
) engine=innodb default character set=utf8 insert_method=last;

create table if not exists Conclusions (
    con_type numeric(8, 0) not null,
    con_conclusion varchar(128) not null,
    constraint pk_conclusions primary key (con_type)
) engine=innodb default character set=utf8 insert_method=last;

create table if not exists Data (
    id numeric(16, 0) not null,
    dt_system_id numeric(8, 0) not null,
    dt_sensor_id numeric(10, 0) not null,
    dt_data numeric(16, 4) not null,
    dt_timestamp datetime not null default now(),
    constraint pk_data primary key (id),
    constraint fk_data_system foreign key (dt_system_id)
        references Systems(id),
    constraint fk_data_sensor foreign key (dt_sensor_id)
        references Sensors(id)
) engine=innodb default character set=utf8 insert_method=last;

create table if not exists Analysis (
    id numeric(20, 0) not null,
    an_data_id numeric(16, 0) not null,
    an_type_id numeric(8, 0) not null,
    an_description varchar(512),
    an_timestamp datetime not null default now(),
    constraint pk_analysis primary key (id),
    constraint fk_analysis_data foreign key (an_data_id) references Data(id),
    constraint fk_analysis_type foreign key (an_type_id)
        references Conclusions(con_type)
) engine=innodb default character set=utf8 insert_method=last;

create table if not exists Services (
    id numeric(4, 0) not null,
    srv_title varchar(128) not null,
    srv_description varchar(512) not null,
    srv_price numeric(8, 4) not null,
    constraint pk_services primary key (id),
    constraint ck_srv_price check(srv_price >= 0),
    constraint uk_title unique (srv_title)
) engine=innodb default character set=utf8 insert_method=last;

create table if not exists Subscription (
    sb_client_id numeric(4, 0) not null,
    sb_service_id numeric(4, 0) not null,
    sb_start_date datetime not null default now(),
    constraint pk_subscription primary key (sb_client_id, sb_service_id),
    constraint fk_subscription_clients foreign key (sb_client_id)
        references Clients(id),
    constraint fk_subscription_services foreign key (sb_service_id)
        references Services(id)
) engine=innodb default character set=utf8 insert_method=last;

create table if not exists Payments (
    id numeric(16, 0) not null,
    pay_client_id numeric(4, 0) not null,
    pay_service_id numeric(4, 0) not null,
    pay_amount numeric(8, 4) not null,
    pay_timestamp datetime not null default now(),
    pay_description varchar(512),
    pay_status varchar(64) not null,
    constraint pk_payments primary key (id),
    constraint fk_payments_subscription
        foreign key (pay_client_id, pay_service_id)
        references Subscription(sb_client_id, sb_service_id),
    constraint ck_pay_amount check(pay_amount > 0),
    constraint ck_pay_status
        check(pay_status in ('complete', 'declined', 'proceeding'))
) engine=innodb default character set=utf8 insert_method=last;
