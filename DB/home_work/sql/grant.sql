use SensorsSystem;

grant select on Services to users;
grant select on SensorsModels to users;
grant select on SystemsModels to users;

grant select, update, insert, delete on Systems to suppliers;
grant select, update, insert, delete on Services to suppliers;
grant select, update, insert, delete on Sensors to suppliers;
grant select, update, insert, delete on SystemsModels to suppliers;
grant select, update, insert, delete on SensorsModels to suppliers;
grant select on Payments to suppliers;
grant select on Clients to suppliers;
grant select on Data to suppliers;
grant select on Conclusions to suppliers;
grant select on Analysis to suppliers;
grant select on Addresses to suppliers;
grant select on Passports to suppliers;
grant select on Phones to suppliers;
grant select on Rent to suppliers;
grant select on Subscription to suppliers;

grant insert on Data to Systems;

grant select, update, insert, delete on Systems to app;
grant select, update, insert, delete on Services to app;
grant select, update, insert, delete on Sensors to app;
grant select, update, insert, delete on SystemsModels to app;
grant select, update, insert, delete on SensorsModels to app;
grant select, insert on Payments to app;
grant select, update, insert, delete on Clients to app;
grant select on Data to app;
grant select, update, insert, delete on Conclusions to app;
grant select, insert, delete on Analysis to app;
grant select, update, insert, delete on Addresses to app;
grant select, update, insert, delete on Passports to app;
grant select, update, insert, delete on Phones to app;
grant select, update, insert, delete on Rent to app;
grant select, update, insert, delete on Subscription to app;
