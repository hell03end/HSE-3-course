/* books - sample of DB, given by lecturer */

CREATE TABLE books(
    id NUMERIC(6, 0) NOT NULL,
    publ VARCHAR(140) NOT NULL,
    p_year DATETIME NOT NULL,
    PRIMARY KEY(id),
) ENGINE=INNODB DEFAULT CHARACTER SET=UTF8;

-- count books by publisher per year
SELECT publ, p_year, COUNT(*)
FROM books
GROUP BY publ, p_year;

-- count people with more then 5 books on hand
SELECT r.name, COUNT(*)
FROM readers AS r, abonements AS a
WHERE r.id = a.reader AND date_out IS NULL
GROUP BY r.name
HAVING COUNT(*) > 5;
-- or
SELECT r.name, COUNT(*)
FROM readers AS r JOIN abonements AS a ON r.id = a.reader
WHERE date_out IS NULL
GROUP BY r.name
HAVING COUNT(*) > 5;

-- select people who took books in this year
SELECT r.name
FROM reader AS r
WHERE r.id NOT IN (
    SELECT reader
    FROM abonements
    WHERE YEAR(NOW() == YEAR(date_in))
);
-- or (faster)
SELECT r.name
FROM reader AS r
WHERE NOT EXISTS(
    SELECT *
    FROM abonements AS a
    WHERE r.id == a.reader AND YEAR(NOW()) == YEAR(date_in)
);

/*
 *  NOT EXISTS (cor) is faster then NOT IN (not cor),
 *  while IN (not cor) is faster then EXISTS (cor),
 *  where (not) cor means correlated
 */

 /* Important: COUNT (or any aggrigation method) doesn't work in WHERE statement */

SELECT r.name
FROM reader as r LEFT JOIN abonements as a ON r.id = a.reader
WHERE YEAR(NOW()) = YEAR(date_in) AND a.reader IS NULL;
-- or
SELECT r.name
FROM reader as r LEFT JOIN abonements as a ON r.id = a.reader
WHERE YEAR(NOW()) = YEAR(date_in)
GROUP BY r.name
HAVING COUNT(reader) = 0;
/* LEFT JOIN is used to count NULL values */
