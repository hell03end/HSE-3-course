/* for Hospital DB (sample, given by lecturer) */

SELECT count(*), pal.num_depart
FROM Palats as pal JOIN Patients as pat ON pal.rid = pat.room
WHERE end_data is NULL
GROUP BY pal.num_depart

-- SELECT all rooms with both F AND M
SELECT room FROM patients group by room having count(sex = 'm')*count(sex = 'f') > 0;
SELECT room FROM patients group by room having count(DISTINCT sex) > 1; -- with DISTINCT
SELECT DISTINCT room FROM patients as p1, patients as p2 WHERE p1.room = p2.room AND p1.sex <> p2.sex

-- SELECT all patients stay more then 20 days AND arrive IN current year
SELECT name FROM patients WHERE end_date - inc_date > 20 AND year(NOW()) = year(inc_date)
