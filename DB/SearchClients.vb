Option Compare Database

Private Sub findButton_Click()

    Dim condstr, S
    condstr = ""

    If (Not IsNull(sName.Value) And (Trim(sName.Value) <> "")) Then
        S = "(firstname LIKE '*" & sName.Value & "*') "
        If condstr <> "" Then
            condstr = condstr & " AND " & S
        Else
            condstr = S
        End If
    End If

    If (Not IsNull(sSurname.Value) And (Trim(sSurname.Value) <> "")) Then
        S = "(surname LIKE '*" & sSurname.Value & "*') "
        If condstr <> "" Then
            condstr = condstr & " AND " & S
        Else
            condstr = S
        End If
    End If

    If (Not IsNull(sLastname.Value) And (Trim(sLastname.Value) <> "")) Then
        S = "(lastname LIKE '*" & sLastname.Value & "*') "
        If condstr <> "" Then
            condstr = condstr & " AND " & S
        Else
            condstr = S
        End If
    End If

    If condstr <> "" Then condstr = "WHERE " & condstr

    AskForClientsSub.Form.RecordSource = "SELECT * FROM AskForClients " & condstr

End Sub
