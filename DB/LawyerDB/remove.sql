/* Read more:
 *  https://dev.mysql.com/doc/refman/5.7/en/drop-database.html
 *  https://dev.mysql.com/doc/refman/5.7/en/drop-table.html
 *  https://dev.mysql.com/doc/refman/5.7/en/drop-view.html
 *  https://dev.mysql.com/doc/refman/5.7/en/drop-index.html
 */

/* Dangerous: fully remove existing database! Operation couldn't be rollbacked. */
-- DROP DATABASE IF EXISTS lawyer;

/* remove ccarticles table in terms of current session (temporary) */
-- DELETE FROM lawyer.ccarticles WHERE TRUE;
DROP TEMPORARY TABLE IF EXISTS lawyer.ccarticles RESTRICT; -- if no references exists
-- DROP TEMPORARY TABLE IF EXISTS lawyer.ccarticles; -- simple remove
-- DROP TEMPORARY TABLE IF EXISTS lawyer.ccarticles CASCADE; -- with all references

/* remove clients table in terms of current session (temporary) */
-- DELETE FROM lawyer.clients WHERE TRUE;
DROP TEMPORARY TABLE IF EXISTS lawyer.clients RESTRICT;
-- DROP TEMPORARY TABLE IF EXISTS lawyer.clients;
-- DROP TEMPORARY TABLE IF EXISTS lawyer.clients CASCADE;

/* remove charges table in terms of current session (temporary) */
-- DELETE FROM lawyer.charges WHERE TRUE;
DROP TEMPORARY TABLE IF EXISTS lawyer.charges RESTRICT;
-- DROP TEMPORARY TABLE IF EXISTS lawyer.charges;
-- DROP TEMPORARY TABLE IF EXISTS lawyer.charges CASCADE;

/* remove cases table in terms of current session (temporary) */
-- DELETE FROM lawyer.cases WHERE TRUE;
DROP TEMPORARY TABLE IF EXISTS lawyer.cases RESTRICT;
-- DROP TEMPORARY TABLE IF EXISTS lawyer.cases;
-- DROP TEMPORARY TABLE IF EXISTS lawyer.cases CASCADE;
