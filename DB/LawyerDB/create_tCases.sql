/* SELECT database to run queries on it */
USE lawyer;

/* create new table */
CREATE TABLE IF NOT EXISTS cases (
    id NUMERIC(6, 0) NOT NULL,
    title VARCHAR(140),
    start_date DATETIME NOT NULL DEFAULT NOW(),
    end_date DATETIME,
    PRIMARY KEY(id),
    CONSTRAINT cas_terms CHECK(end_date > start_date)
) ENGINE=INNODB DEFAULT CHARACTER SET=UTF8 INSERT_METHOD=LAST;
