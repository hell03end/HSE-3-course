/*
 *  read more:
 *  datetime arithmetic: https://dev.mysql.com/doc/refman/5.7/en/date-and-time-functions.html
 */

/* add data to ccarticles (id, title, min_term, max_term) */
INSERT INTO ccarticles(id, title, min_term, max_term) VALUES (1, NULL, 0, 1);
INSERT INTO ccarticles(id, title, min_term, max_term) VALUES (2, 'murder', 1, 10);
INSERT INTO ccarticles(id, title, min_term, max_term) VALUES (3, NULL, 0, 2.5);
INSERT INTO ccarticles(id, title, min_term, max_term) VALUES (4, NULL, 0, 2);
INSERT INTO ccarticles(id, title, min_term, max_term) VALUES (5, 'murder', 3, 15);
INSERT INTO ccarticles(id, title, min_term, max_term) VALUES (6, NULL, 2, 3);
INSERT INTO ccarticles(id, title, min_term, max_term) VALUES (7, 'murder', 1, 5);

/* add data to cases (id, title, start_date, end_date) */
INSERT INTO cases(id, title, start_date, end_date)
    VALUES (1, 'murder', NOW() - INTERVAL 24 DAY, NOW() - INTERVAL 12 DAY);
INSERT INTO cases(id, title, start_date, end_date)
    VALUES (2, NULL, NOW() - INTERVAL 31 DAY, NOW() - INTERVAL 1 DAY);
INSERT INTO cases(id, title, start_date, end_date)
    VALUES (3, 'murder', NOW(), NULL);
INSERT INTO cases(id, title, start_date, end_date)
    VALUES (4, NULL, NOW() - INTERVAL 31 DAY, NOW() - INTERVAL 1 DAY);
INSERT INTO cases(id, title, start_date, end_date)
    VALUES (5, 'murder', NOW() - INTERVAL 31 DAY, NOW() - INTERVAL 1 DAY);
INSERT INTO cases(id, title, start_date, end_date)
    VALUES (6, NULL, NOW() - INTERVAL 31 DAY, NOW() - INTERVAL 1 DAY);
INSERT INTO cases(id, title, start_date, end_date)
    VALUES (7, 'murder', NOW(), NULL);

/* add data to clients (case_id, passport, firstname, surname, lastname, birth, comment, chamber, fee, outcome, term) */
INSERT INTO clients(case_id, passport, firstname, surname, lastname, birth, comment, chamber, fee, outcome, term)
    VALUES (1, '12312312341231231234', 'Alex', 'DeLarge', NULL, now() - INTERVAL 20 YEAR, 'illegal immigrant', 1, 100, NULL, NULL);
INSERT INTO clients(case_id, passport, firstname, surname, lastname, birth, comment, chamber, fee, outcome, term)
    VALUES (2, '12312312341231231235', 'Susan', 'Susan', 'Susan', now() - INTERVAL 14 YEAR, NULL, 2, 0, 'sentenced conditionally', 1);
INSERT INTO clients(case_id, passport, firstname, surname, lastname, birth, comment, chamber, fee, outcome, term)
    VALUES (3, '12312312341231231236', 'Jake', 'Dog', NULL, now() - INTERVAL 40 YEAR, 'recidivist', 3, 99, 'justified', 0);
INSERT INTO clients(case_id, passport, firstname, surname, lastname, birth, comment, chamber, fee, outcome, term)
    VALUES (4, '12312312341231231237', 'Alex', 'Dog', NULL, now() - INTERVAL 40 YEAR, 'recidivist', 3, 99, 'sentenced conditionally', 1);
INSERT INTO clients(case_id, passport, firstname, surname, lastname, birth, comment, chamber, fee, outcome, term)
    VALUES (5, '12312312341231231238', 'Finn', 'Dog', NULL, now() - INTERVAL 40 YEAR, 'recidivist', 3, 99, 'justified', 0);
INSERT INTO clients(case_id, passport, firstname, surname, lastname, birth, comment, chamber, fee, outcome, term)
    VALUES (6, '12312312341231231239', 'Jake', 'DeLarge', NULL, now() - INTERVAL 40 YEAR, 'recidivist', 3, 99, 'sentenced conditionally', 2);
INSERT INTO clients(case_id, passport, firstname, surname, lastname, birth, comment, chamber, fee, outcome, term)
    VALUES (7, '12312312341231231230', 'Susan', 'Susan', NULL, now() - INTERVAL 40 YEAR, 'recidivist', 3, 99, 'justified', 0);
INSERT INTO clients(case_id, passport, firstname, surname, lastname, birth, comment, chamber, fee, outcome, term)
    VALUES (2, '12312312341231231223', 'Susan', 'Susan', 'Susan', now() - INTERVAL 14 YEAR, NULL, 2, 0, 'sentenced conditionally', 1);

/* add data to charges(id, client_id, article_id) */
INSERT INTO charges(id, client_id, article_id) VALUES (1, '12312312341231231235', 3);
INSERT INTO charges(id, client_id, article_id) VALUES (2, '12312312341231231236', 1);
INSERT INTO charges(id, client_id, article_id) VALUES (3, '12312312341231231234', 2);
INSERT INTO charges(id, client_id, article_id) VALUES (4, '12312312341231231238', 7);
INSERT INTO charges(id, client_id, article_id) VALUES (5, '12312312341231231237', 6);
INSERT INTO charges(id, client_id, article_id) VALUES (6, '12312312341231231239', 5);
INSERT INTO charges(id, client_id, article_id) VALUES (7, '12312312341231231239', 4);
