/* Read more:
 *  https://dev.mysql.com/doc/refman/5.7/en/SHOW.html
 *  https://dev.mysql.com/doc/refman/5.7/en/SELECT.html
 */

/* display all existing databases */
-- SHOW DATABASES;

/* display existing tables */
-- SHOW TABLES FROM lawyer;

/* display info about columns in tables */
SHOW COLUMNS FROM ccarticles FROM lawyer WHERE TRUE; -- or like
SHOW COLUMNS FROM clients FROM lawyer WHERE TRUE; -- or like
SHOW COLUMNS FROM charges FROM lawyer WHERE TRUE; -- or like
SHOW COLUMNS FROM cases FROM lawyer WHERE TRUE; -- or like

/* display full info about columns in tables */
-- SHOW FULL COLUMNS FROM ccarticles FROM lawyer WHERE TRUE; -- or like
-- SHOW FULL COLUMNS FROM clients FROM lawyer WHERE TRUE; -- or like
-- SHOW FULL COLUMNS FROM charges FROM lawyer WHERE TRUE; -- or like
-- SHOW FULL COLUMNS FROM cases FROM lawyer WHERE TRUE; -- or like

/* display full info about indexes IN tables */
-- SHOW index FROM ccarticles FROM lawyer;
-- SHOW index FROM clients FROM lawyer;
-- SHOW index FROM charges FROM lawyer;
-- SHOW index FROM cases FROM lawyer;

/* display unique content of particular tables */
-- SELECT DISTINCT SQL_NO_CACHE * FROM lawyer.ccarticles LIMIT 10;
-- SELECT DISTINCT SQL_NO_CACHE * FROM lawyer.clients LIMIT 10;
-- SELECT DISTINCT SQL_NO_CACHE * FROM lawyer.charges LIMIT 10;
-- SELECT DISTINCT SQL_NO_CACHE * FROM lawyer.cases LIMIT 10;

/* display content of particular tables */
-- SELECT SQL_NO_CACHE * FROM lawyer.ccarticles LIMIT 10;
-- SELECT SQL_NO_CACHE * FROM lawyer.clients LIMIT 10;
-- SELECT SQL_NO_CACHE * FROM lawyer.charges LIMIT 10;
-- SELECT SQL_NO_CACHE * FROM lawyer.cases LIMIT 10;

/* display specific qeries */
-- SELECT * FROM lawyer.clients;
