/*
 *  operations: https://dev.mysql.com/doc/refman/5.7/en/comparison-operators.html
 *  UNIQUE: https://www.w3schools.com/sqL/sql_unique.asp
 */

/* SELECT database to run queries on it */
USE lawyer;

/* create new table */
CREATE TABLE IF NOT EXISTS clients (
    case_id NUMERIC(6, 0) NOT NULL,
    passport CHAR(20) NOT NULL,
    firstname VARCHAR(20) NOT NULL,
    surname VARCHAR(20) NOT NULL,
    lastname VARCHAR(20),
    birth DATETIME NOT NULL,
    comment VARCHAR(20),
    chamber NUMERIC(3, 0),
    fee NUMERIC(8, 2) NOT NULL DEFAULT 0,
    outcome VARCHAR(40),
    term NUMERIC(4, 1),
    PRIMARY KEY(passport),
    FOREIGN KEY (case_id) REFERENCES cases(id),
    CONSTRAINT cl_birth CHECK(birth BETWEEN DATE('1920-01-01') AND DATE(NOW())),
    CONSTRAINT cl_chamber CHECK(chamber > 0),
    CONSTRAINT cl_fee CHECK(fee >= 0),
    CONSTRAINT cl_comment
        CHECK(COMMENT IN ('recidivist', 'foreign citizen', 'illegal immigrant')),
    CONSTRAINT cl_outcome
        CHECK(outcome IN ('justified', 'sentenced', 'sentenced conditionally')),
    CONSTRAINT cl_term
        CHECK((term = 0 AND outcome = 'justified') OR (term > 0 AND outcome <> 'justified'))
) ENGINE=INNODB DEFAULT CHARACTER SET=UTF8 INSERT_METHOD=LAST;
