import json
import os
import random
from argparse import ArgumentParser, Namespace
from datetime import datetime
import pprint
from time import sleep

import pymysql.cursors


def load_json(path: str) -> (dict, None):
    if os.path.exists(path):
        with open(path, 'r') as reader:
            return json.load(reader)


def parse_argv() -> Namespace:
    parser = ArgumentParser(description="Handle actions with database.")
    parser.add_argument('action', type=str, help="action to run on database")
    parser.add_argument('--path', type=str,
                        default="templates.json", help="path to templates")
    parser.add_argument('--user', '-u', type=str,
                        default="root", help="username")
    parser.add_argument('--password', '-p', type=str,
                        default=None, help="password")
    parser.add_argument('--db', type=str, default="lawyer", help="db name")
    parser.add_argument('--host', type=str,
                        default="localhost", help="host name")
    parser.add_argument('--verbose', '-v', type=bool,
                        default=False, help="verbose output")
    parser.add_argument('--pretty', type=bool, default=False,
                        help="use PrettyPrinter to show db")
    return parser.parse_args()


def get_templates(data: dict) -> dict:
    if not isinstance(data, dict):
        raise ValueError("Expect dict, got {}".format(type(data)))
    return {
        'cl': data['ins']['cl']['a'],
        'cc': data['ins']['cc']['a'],
        'ch': data['ins']['ch']['a'],
        'cas': data['ins']['cas']['a'],
        'del': data['del']['a'],
        'shwa': data['shw']['a']
    }


def execute(connection: pymysql.connections.Connection,
            query: str, verbose: bool=False, **kwargs) -> None:
    with connection.cursor() as cursor:
        if verbose:
            print(query)
        cursor.execute(query)
    connection.commit()


def show(connection: pymysql.connections.Connection,
         query: str, verbose: bool=False, **kwargs) -> None:
    pretty = kwargs.get("pretty", False)
    if pretty:
        pp = pprint.PrettyPrinter(compact=True, depth=2)
    with connection.cursor() as cursor:
        if verbose:
            print(query)
        cursor.execute(query)
        for row in cursor.fetchall():
            if pretty:
                pp.pprint(row)
            else:
                print(row)


def fill_ccarticles(connection: pymysql.connections.Connection, template: str,
                    data: list=None, verbose: bool=False, **kwargs) -> None:
    if not data:
        return
    for i in range(kwargs.get('count', 100)):
        term = random.randint(0, 5)
        query = template.format("{}, {}, {}, {}".format(
            i + 1,
            data['title'][random.randint(0, len(data['title']) - 1)],
            term,
            term + random.randint(0, 10)
        ))
        execute(connection, query, verbose, **kwargs)


def fill_cases(connection: pymysql.connections.Connection, template: str,
               data: list=None, verbose: bool=False, **kwargs) -> None:
    if not data:
        return
    for i in range(kwargs.get('count', 500)):
        tt = (
            datetime.utcnow().timestamp() - 60 * random.randint(30, 60) *
            random.randint(12, 24) * random.randint(255, 365) *
            random.randint(0, 3)
        )
        t = datetime.utcfromtimestamp(tt)
        tt += (
            60 * random.randint(30, 60) * random.randint(12, 24) *
            random.randint(255, 365) * random.randint(0, 3)
        )
        t1 = datetime.utcfromtimestamp(tt)
        if not random.randint(0, 10):
            t1 = "NULL"
        else:
            t1 = "\'{:.19}\'".format(str(t1))
        query = template.format("{}, {}, {}, {}".format(
            i + 1,
            data['title'][random.randint(0, len(data['title']) - 1)],
            "\'{:.19}\'".format(str(t)),
            t1
        ))
        execute(connection, query, verbose, **kwargs)


def fill_clients(connection: pymysql.connections.Connection, template: str,
                 data: list=None, verbose: bool=False, **kwargs) -> None:
    if not data:
        return
    passport = data['id']
    for idx in range(kwargs.get('count', 1000)):
        outcome = data['outcome'][random.randint(0, len(data['outcome']) - 1)],
        outcome = outcome[0]
        term = random.randint(0, 5)
        if outcome == "'justified'":
            term = 0
        elif outcome == "NULL":
            term = "NULL"
        tt = (
            datetime.utcnow().timestamp() - 60 * 60 * random.randint(12, 24) *
            random.randint(255, 365) * random.randint(18, 64)
        )
        t = datetime.utcfromtimestamp(abs(tt))
        query = template.format(
            "{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}".format(
                random.randint(kwargs.get('min_case', 1),
                               kwargs.get('max_case', 500)),  # case id
                "\'{:0<20.20}\'".format(str(passport + idx)),  # passport
                data['fname'][random.randint(0, len(data['fname']) - 1)],
                data['sname'][random.randint(0, len(data['sname']) - 1)],
                data['lname'][random.randint(0, len(data['lname']) - 1)],
                "\'{:.19}\'".format(str(t)),  # birth
                data['comment'][random.randint(0, len(data['comment']) - 1)],
                random.randint(0, 255),  # chamber
                random.randint(0, 10000),  # fee
                outcome,
                term
            )
        )
        execute(connection, query, verbose, **kwargs)


def fill_charges(connection: pymysql.connections.Connection, template: str,
                 data: list=None, verbose: bool=False, **kwargs) -> None:
    if not data:
        return
    passport = data['id']
    for idx in range(kwargs.get('count', 1000)):
        query = template.format("{}, {}, {}".format(
            idx + 1,
            "\'{:0<20.20}\'".format(str(passport + idx)),  # client_id
            random.randint(1, kwargs.get('cc_count', 100))  # article_id
        ))
        execute(connection, query, verbose, **kwargs)


if __name__ == "__main__":
    args = parse_argv()
    try:
        templates = get_templates(load_json(args.path))
    except ValueError as err:
        print(err)
        exit(1)
    action = args.action
    password = args.password
    if not password:
        password = os.environ.get('MYSQL_R_PASS', "DBpass123")
    connection = pymysql.connect(
        host=args.host,
        user=args.user,
        password=password,
        db=args.db,
        charset='utf8',  # 'utf8mb4'
        cursorclass=pymysql.cursors.DictCursor
    )
    params = {
        'connection': connection,
        'verbose': args.verbose
    }
    artifacts = load_json("artifacts.json")

    try:
        # ccarticles == cc
        if action == "fa" or action == "fcc":
            fill_ccarticles(
                template=templates['cc'],
                data=artifacts['cc'],
                **params
            )
        elif action == "sa" or action == "scc":
            show(query=templates['shwa'].format("*", "ccarticles"),
                 pretty=args.pretty, **params)
        elif action == "da" or action == "dcc":
            execute(query=templates['del'].format("ccarticles", "true"),
                    **params)

        # cases = cas
        if action == "fa" or action == "fcas":
            fill_cases(
                template=templates['cas'],
                data=artifacts['cas'],
                **params
            )
        elif action == "sa" or action == "scas":
            show(query=templates['shwa'].format("*", "cases"),
                 pretty=args.pretty, **params)
        elif action == "da" or action == "dcas":
            execute(query=templates['del'].format("cases", "true"), **params)

        # clients = cl
        if action == "fa" or action == "fcl":
            fill_clients(
                template=templates['cl'],
                data=artifacts['cl'],
                **params
            )
        elif action == "sa" or action == "scl":
            show(query=templates['shwa'].format("*", "clients"),
                 pretty=args.pretty, **params)
        elif action == "da" or action == "dcl":
            execute(query=templates['del'].format("clients", "true"), **params)

        # charges = ch
        if action == "fa" or action == "fch":
            fill_charges(
                template=templates['ch'],
                data=artifacts['cl'],
                **params
            )
        elif action == "sa" or action == "sch":
            show(query=templates['shwa'].format("*", "charges"),
                 pretty=args.pretty, **params)
        elif action == "da" or action == "dch":
            execute(query=templates['del'].format("charges", "true"), **params)
    finally:
        connection.close()
