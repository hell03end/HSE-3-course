/*
 * Read more:
 *  https://dev.mysql.com/doc/refman/5.7/en/create-database.html
 *  https://dev.mysql.com/doc/refman/5.7/en/USE.html
 *  https://dev.mysql.com/doc/refman/5.7/en/create-table.html
 */

/* create new plain database */
CREATE DATABASE IF NOT EXISTS lawyer DEFAULT CHARACTER SET UTF8 DEFAULT COLLATE utf8_general_ci;
