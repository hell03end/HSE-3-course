/*
 *  Для созданных представлений необходимо проверить с помощью запросов UPDATE,
 *  DELETE и INSERT, являются ли они обновляемыми, и объяснить полученный
 *  результат.
 */

-- Conditionally sentenced clients with term <= 1 year
CREATE VIEW short_term(name, fee, outcome, term) -- modified
AS SELECT CONCAT(firstname, ' ', surname, ' ', lastname), fee, outcome, term
    FROM clients AS cl
    WHERE outcome = 'sentenced conditionally' AND term <= 3;
-- SELECT * FROM short_term;
-- DROP VIEW short_term;

-- "Эффективность защиты": дело – ФИО – (максимальный срок - срок по приговору)
--  – (срок по приговору - минимальный срок).
-- Минимальный и максимальный сроки должны выбираться среди всех статей,
-- по которым обвинялся клиент в рамках одного дела.
CREATE VIEW eff_protect(case_n, cl_name, delta_max, delate_min)
AS SELECT cas.title, CONCAT(cl.firstname, ' ', cl.surname, ' ', cl.lastname),
        cc.max_term - cl.term, cl.term - cc.min_term
    FROM cases AS cas
    JOIN clients AS cl ON cl.case_id = cas.id
    JOIN charges AS ch ON cl.passport = ch.client_id
    JOIN ccarticles as cc ON ch.article_id = cc.id;
-- SELECT * FROM eff_protect;
-- DROP VIEW eff_protect;

-- "Articles list": case number – CC article number
CREATE VIEW articles(case_n, article_n) -- unmodified (as distinct)
AS SELECT DISTINCT cas.id, cc.id
    FROM cases AS cas
    JOIN clients AS cl ON cl.case_id = cas.id
    JOIN charges AS ch ON cl.passport = ch.client_id
    JOIN ccarticles as cc ON ch.article_id = cc.id;
-- SELECT * from articles;
-- DROP VIEW articles;
