SOURCE remove.sql; -- remove old tables
SOURCE create_db.sql; -- create new database if not exists

-- create tables
SOURCE create_tCCArticles.sql;
SOURCE create_tCases.sql;
SOURCE create_tClients.sql;
SOURCE create_tCharges.sql;

-- SOURCE fill.sql; -- add data to tables
SOURCE show.sql; -- display results
