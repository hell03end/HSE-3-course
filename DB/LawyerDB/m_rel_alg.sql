/* project */
SELECT DISTINCT id, title FROM cases;
SELECT COUNT(DISTINCT id, title) FROM cases;

/* select */
SELECT * FROM cases WHERE end_date > NOW();
SELECT COUNT(*) FROM cases WHERE end_date > NOW();

/* сartesian product */
SELECT * FROM cases, ccarticles;
SELECT COUNT(DISTINCT cas.id)*COUNT(DISTINCT cc.id) = COUNT(*)
FROM cases AS cas, ccarticles AS cc;

/* union */
-- create additional views
CREATE VIEW old_cases(id, start_date, end_date)
AS SELECT DISTINCT id, start_date, end_date FROM cases WHERE end_date < NOW();
CREATE VIEW new_cases(id, start_date, end_date)
AS SELECT DISTINCT id, start_date, end_date FROM cases WHERE end_date > NOW();
-- realize operation
SELECT * FROM old_cases UNION SELECT * FROM new_cases;
SELECT COUNT(*) FROM SELECT * FROM old_cases UNION SELECT * FROM new_cases AS uc;
-- SELECT COUNT(DISTINCT oc.id) + COUNT(DISTINCT nc.id) = COUNT(DISTINCT uc.id)
-- FROM old_cases AS oc, new_cases AS nc,
--     (SELECT * FROM old_cases UNION SELECT * FROM new_cases) AS uc;
-- remove additional views
DROP VIEW old_cases;
DROP VIEW new_cases;

/* except */
-- create additional views
CREATE VIEW a(id, start_date, end_date)
AS SELECT id, start_date, end_date FROM cases LIMIT 200;
CREATE VIEW b(id, start_date, end_date)
AS SELECT id, start_date, end_date FROM cases LIMIT 50;
-- realize operation
SELECT COUNT(*) = 150 FROM a WHERE id NOT IN (SELECT id FROM b);
-- remove additional views
DROP VIEW a;
DROP VIEW b;

/* intersect */
CREATE VIEW a(id, start_date, end_date)
AS SELECT id, start_date, end_date FROM cases LIMIT 200;
CREATE VIEW b(id, start_date, end_date)
AS SELECT id, start_date, end_date FROM cases LIMIT 50;
-- realize operation
SELECT COUNT(*) = 50 FROM a WHERE id IN (SELECT id FROM b);
-- remove additional views
DROP VIEW a;
DROP VIEW b;

/* join */
SELECT * FROM cases AS cas JOIN clients AS cl ON cl.case_id = cas.id;
SELECT COUNT(*) = 1000
FROM cases AS cas JOIN clients AS cl ON cl.case_id = cas.id;
