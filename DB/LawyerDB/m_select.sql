/* V12 */

use lawyer;

-- Check there are no accused for similar cases in same chamber.
SELECT chamber AS `room`, case_id AS `case`, COUNT(passport) AS `number`
FROM clients
GROUP BY chamber, case_id
HAVING COUNT(passport) > 1
ORDER BY chamber, case_id;
-- or
SELECT DISTINCT cl1.chamber AS `room`, cl1.case_id AS `case`
FROM clients AS cl1 RIGHT JOIN clients AS cl2 ON cl1.chamber = cl2.chamber
WHERE cl1.chamber = cl2.chamber AND cl1.case_id = cl2.case_id AND
    cl1.passport <> cl2.passport
ORDER BY cl1.chamber, cl1.case_id;

-- Count sum of fee for cases, which have ended this year.
SELECT SUM(cl.fee) AS `income`
FROM clients AS cl INNER JOIN cases AS cas ON cl.case_id = cas.id
WHERE YEAR(cas.end_date) = YEAR(NOW());
-- or
SELECT SUM(cl.fee) AS `income`
FROM clients AS cl
WHERE YEAR(NOW()) = YEAR((
    SELECT end_date
    FROM cases as cas
    WHERE cas.id = cl.case_id
));

/* Create lists ordered by start date */
-- a. defendants (by cases);
SELECT cas.id AS `case`, cas.title, cas.start_date AS `start date`,
    CONCAT(cl.firstname, ' ', cl.surname) AS `full name`
FROM clients as cl JOIN cases AS cas ON cl.case_id = cas.id
GROUP BY cas.id, cas.start_date, cas.title, cl.firstname, cl.surname
ORDER BY cas.start_date, cas.id, cas.title
LIMIT 10;

-- b. juvenile defendants;
SELECT CONCAT(cl.firstname, ' ', cl.surname) AS `full name`, cl.birth AS `birth`,
    YEAR(NOW()) - YEAR(cl.birth) AS `age`, cas.title
FROM clients AS cl INNER JOIN cases AS cas ON cl.case_id = cas.id
WHERE YEAR(NOW()) - YEAR(cl.birth) < 18
ORDER BY cl.birth, cas.title;
-- or
SELECT CONCAT(cl.firstname, ' ', cl.surname) AS `full name`, cl.birth AS `birth`,
    YEAR(NOW()) - YEAR(cl.birth) AS `age`, cas.title
FROM clients AS cl INNER JOIN cases AS cas ON cl.case_id = cas.id
WHERE YEAR(cas.start_date) - YEAR(cl.birth) < 18;

-- c. defendants for cases with max term >= 10 years.
SELECT CONCAT(cl.firstname, ' ', cl.surname) AS `full name`, cc.max_term
FROM clients AS cl INNER JOIN charges AS ch ON cl.passport = ch.client_id
    INNER JOIN ccarticles AS cc ON ch.article_id = cc.id
WHERE cc.max_term >= 10
ORDER BY cc.max_term;
-- or
SELECT CONCAT(cl.firstname, ' ', cl.surname) AS `full name`
FROM clients AS cl
WHERE passport in (
    SELECT client_id
    FROM charges
    WHERE article_id in (SELECT id FROM ccarticles WHERE max_term >= 10)
);

-- select all ccarticles with max term > 10
SELECT *
FROM ccarticles
WHERE max_term >= 10;
-- select all charges for ccarticles with max term > 10
SELECT *
FROM charges AS ch JOIN ccarticles AS cc ON ch.article_id = cc.id
WHERE cc.max_term >= 10
ORDER BY ch.id, ch.article_id;
-- or
SELECT *
FROM charges
WHERE article_id in (SELECT id FROM ccarticles WHERE max_term >= 10)
ORDER BY id, article_id;
-- or
SELECT DISTINCT *
FROM charges AS ch JOIN (SELECT * FROM ccarticles WHERE max_term >= 10) AS cc
    ON ch.article_id = cc.id
WHERE ch.article_id = cc.id
ORDER BY ch.id, ch.article_id;

-- count number of clients for different cases
SELECT title, COUNT(*)
FROM clients as cl INNER JOIN cases AS cas ON cl.case_id = cas.id
GROUP BY cas.title;
