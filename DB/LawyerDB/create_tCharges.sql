/* SELECT database to run queries on it */
USE lawyer;

/* create new table */
CREATE TABLE IF NOT EXISTS charges (
    id NUMERIC(20, 0) NOT NULL,
    client_id CHAR(20) NOT NULL,
    article_id NUMERIC(4, 0) NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (client_id) REFERENCES clients(passport),
    FOREIGN KEY (article_id) REFERENCES ccarticles(id)
) ENGINE=INNODB DEFAULT CHARACTER SET=UTF8 INSERT_METHOD=LAST;
