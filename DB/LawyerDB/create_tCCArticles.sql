/* SELECT database to run queries on it */
USE lawyer;

/* create new table */
CREATE TABLE IF NOT EXISTS ccarticles (
    id NUMERIC(4, 0) NOT NULL,
    title VARCHAR(140),
    min_term NUMERIC(4, 1) DEFAULT 0,
    max_term NUMERIC(4, 1),
    PRIMARY KEY(id),
    CONSTRAINT cc_terms CHECK(min_term > max_term)
) ENGINE=INNODB DEFAULT CHARACTER SET=UTF8 INSERT_METHOD=LAST;
