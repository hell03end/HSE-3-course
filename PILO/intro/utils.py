import logging
from typing import Generator

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d]"
           " %(message)s",
    datefmt="%H:%M:%S"
)


def proceed_input(processor: Generator) -> None:
    """ Proceed input with given char processor """
    logging.debug("Waiting for input...")
    count = 0  # input string counter

    try:
        while True:  # for multiline processing
            idx = 0
            for idx, char in enumerate(input()):
                logging.debug("Proceeding '%s'...", char)
                processor.send(((count, idx), char))
            # for correct 11->0 processing
            logging.debug("Proceeding '\\n'...")
            processor.send(((count, idx + 1), '\n'))
            count += 1
    except StopIteration:
        logging.debug("String processor exit.")
    except KeyboardInterrupt:
        logging.info("Program was interupted by user.")
