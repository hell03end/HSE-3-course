"""
    Построить конечный автомат для распознавания цепочек,
    которые представляют собой комментарии в Паскале.

    Согласно https://www.tutorialspoint.com/pascal/pascal_basic_syntax.htm,
    комментарии в Паскале имеют следующих синтаксис:
        * `{ comment }` - однострочный комментарий;
        * `{* comment *}` - многострочный комментарий.
    Вложенные комментарии в Паскале запрещены,
    поэтому не учтены при выполнении данного задания.

    by Dmitriy Pchelkin @hell03end
"""

from typing import Iterable, Iterator, Tuple

from utils import logging, proceed_input


def state_machine_pascal_comments() -> Iterator[bool]:
    """
        Finite state maching for searching pascal comments

        Throws StopIteration on stop of processing.

        States
        ------
        Common case:
        |   | { | } | * | s | n |
        |---|---|---|---|---|---|
        | I | A | I | I | I | I |
        | A | B | Z | C | B | I |
        | B | B | Z | B | B | I |
        | C | D | Z | E | D | D |
        | D | D | D | E | D | D |
        | E | D | Z | E | D | D |
        | Z | A | I | I | I | I |
        where:
        I - initial state.
        A - found '{' - start of any comment.
        B - waiting for singleline-comment end (on '}').
        C - multiline-comment middle stage (it can be '{*}'-string).
        D - waiting for multiline-comment end start on '*'.
        E - wait for '}' for multiline-comment end.
        Z - comment found.

        Usage
        -----
        >>> sm = state_machine_pascal_comments()
        >>> next(sm)
        >>> sm.send(("{", 0))
    """
    def go_to_state(c: str, state: str, pattern: str=None) -> bool:
        if not pattern or c in pattern:
            logging.debug("Change state to %s.", state)
            return True
        return False

    idx, char = yield
    while True:
        if go_to_state(char, "A", "{"):
            # ===== State A =====
            idx, char = yield
            if go_to_state(char, "Z", "}"):
                logging.info("Comment found on %s!", idx)
            elif go_to_state(char, "C", "*"):
                # ===== State C =====
                idx, char = yield
                if go_to_state(char, "Z", "}"):
                    logging.info("Comment found on %s!", idx)
                go_to_state(char, "D")
                while True:
                    # ===== State D =====
                    if go_to_state(char, "E", "*"):
                        # ===== State E =====
                        while go_to_state(char, "E", "*"):
                            idx, char = yield
                        if go_to_state(char, "Z", "}"):
                            logging.info("Comment found on %s!", idx)
                            break
                        go_to_state(char, "D")
                    else:
                        idx, char = yield
            elif go_to_state(char, "I", "\n"):
                pass
            elif go_to_state(char, "B"):
                # ===== State B =====
                idx, char = yield
                while char not in "}\n":
                    idx, char = yield
                if go_to_state(char, "I", "\n"):
                    pass
                elif go_to_state(char, "Z", "}"):
                    logging.info("Comment found on %s!", idx)
            idx, char = yield
        else:
            idx, char = yield


if __name__ == "__main__":
    logging.debug("Start of \"%s\".", __file__)

    STRING_PROCESSOR = state_machine_pascal_comments()
    next(STRING_PROCESSOR)  # activate generator
    proceed_input(STRING_PROCESSOR)

    logging.debug("End of \"%s\".", __file__)
