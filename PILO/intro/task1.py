"""
    Построить программу конечного распознавателя строк,
    содержащих цепочку "110", для языка, состоящего из нулей и единиц.

    by Dmitriy Pchelkin @hell03end
"""

from typing import Iterable, Iterator

from utils import logging, proceed_input


def state_machine_110(alphabet: frozenset={"0", "1", "\n"}) -> Iterator[bool]:
    """
        Finite state maching for searching 110-sequencies
        in {0, 1, \n}-alphabet strings

        Throws StopIteration on stop of processing.

        States
        ------
        Common case:
            I -(1)-> A -(1)-> B -(0)-> Z
                              B -(1)-> B,
        where:
        I - proceed any input
        A - found 1
        B - found 11

        Usage
        -----
        >>> sm = state_machine_110()
        >>> next(sm)
        >>> sm.send(("0", 0))
    """

    def go_to_state(c: str, state: str, pattern: str) -> bool:
        if c in alphabet and c in pattern:
            logging.debug("Change state to %s.", state)
            return True
        return False

    idx, char = yield
    while char in alphabet:
        if go_to_state(char, "A", pattern="1"):
            idx, char = yield
            if go_to_state(char, "B", pattern="1"):
                idx, char = yield
                while go_to_state(char, "B", pattern="1"):
                    idx, char = yield
                if go_to_state(char, "I", pattern="0"):
                    logging.info("Found '110' on %s!", idx)
                    idx, char = yield
        else:
            idx, char = yield

    logging.debug("'%s' (%s) is not from '%s'!", char, type(char), alphabet)


if __name__ == "__main__":
    logging.debug("Start of \"%s\".", __file__)

    STRING_PROCESSOR = state_machine_110()
    next(STRING_PROCESSOR)  # activate generator
    proceed_input(STRING_PROCESSOR)

    logging.debug("End of \"%s\".", __file__)
