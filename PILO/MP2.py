"""
    Построить непримитивный МП-автомат
    (постараться найти автомат с одним состоянием),
    который будет выполнять следующие переводы:
        0^(2n) 1^(3n) 1^m в 0^m 1^n, n>0, m>0.
"""

import logging

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d]"
           " %(message)s",
    datefmt="%H:%M:%S"
)

ALPHABET = {"0", "1"}


def transform_pattern(pattern: str) -> str or None:
    """
        Transform pattern:
            from:   0^(2n) 1^(3n) 1^m
            to:     0^m 1^n
            with:   n>0, m>0
    """
    logging.debug("Proceed pattern:\t'%s'", pattern)
    STATE = "A"
    stack_a = [STATE, STATE, STATE]
    stack_b = []
    res = []
    for char in pattern:
        logging.debug("Proceed:\t%c\t%s\t%s\t%s", char, stack_a, stack_b, res)
        if char not in ALPHABET:
            logging.warning("Found invalid char\t`%c`", char)
            return
        if char == "0" and stack_a:
            if not stack_b:
                stack_b.append(STATE)
            _ = stack_a.pop()
            _ = stack_a.pop()
            if not stack_a:  # each second 0
                res.insert(0, "1")
                stack_a.extend([STATE, STATE])
                stack_b.extend([STATE])
            stack_a.append(STATE)
            stack_b.append(STATE)
        elif char == "1" and stack_b:
            if stack_a and len(stack_a) == 3:
                stack_a = []
            elif stack_a:
                break
            _ = stack_b.pop()
            if not stack_b:
                res.insert(0, "0")
        elif char == "1" and not stack_a and not stack_b:
            res.insert(0, "0")
        else:
            logging.debug("Invalid state!")
            return ""
    if stack_b or stack_a:
        logging.debug("Invalid state!")
        return ""
    return "".join(res)


if __name__ == "__main__":
    logging.debug("Start of \"%s\".", __file__)

    invalid_patterns = (
        "",
        "0",
        "00",
        "000",
        "000",
        "1",
        "11",
        "111",
        "011",
        "001",
        "0011",
        "00110",
        "01011",
        "11100",
        "11100",
        "11110",
        "01010",
        "000011111",
        "000011111101",
        "00111",
        "0000111111",
        "000000111111111"
    )

    patterns = (
        "001111",
        "0011111",
        "00111111",
        "00001111111",
        "000011111111",
        "0000111111111",
        "0000001111111111"
    )

    answers = (
        "01",
        "001",
        "0001",
        "011",
        "0011",
        "00011",
        "0111"
    )

    for idx, pattern in enumerate(invalid_patterns):
        logging.debug("%d\t%s", idx, pattern)
        res = transform_pattern(pattern)
        assert isinstance(res, str)
        assert not res

    for idx, (pattern, ans) in enumerate(zip(patterns, answers)):
        logging.debug("%d\t%s\t%s", idx, pattern, ans)
        res = transform_pattern(pattern)
        assert isinstance(res, str)
        assert res == ans

    # while True:
    #     pattern = input(f"Enter pattern from {ALPHABET}:\t").strip()
    #     result = transform_pattern(pattern)
    #     if result is None:
    #         print("EXIT")
    #         break
    #     else:
    #         print("OUT:", result if result else "INVALID", sep="\t")

    logging.debug("End of \"%s\".", __file__)
