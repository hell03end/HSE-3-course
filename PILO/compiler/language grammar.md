python3 code should be generated from special files with yaml-like syntax:
```yaml
# instructures should be presented as illustrated bellow: order matters!

import:  # imports python modules for custom functions e.g.: Levenshtein distance [optional]
    # format: <name of module in code>: <path to module>
    # ...
    cool_feature: my_module.py

config: # define global configs [optional]
    seed: 42 # random seed for all random functions calls

load: # load data from file to variable
    # format: - <variable name>: <path to file>
    # ...
    # after this step <variable name>.<feature name> is allowed to access specific feature
    - x: path_to_x.csv
    - y: path_to_y.csv

select: # select certain features from datasets [optional]
    # format: <variable name>:
    # - <feature name>
    # - ...
    x:
    - data

split: # train test split: creates both <variable name>.train, <variable name>.test variables [optional]
    # format: <variable name>.<modifier>: <size (relative)>
    # ...
    # modifier = train/test
    x.train: 0.7
    # x.test: 0.4 -- error: only one declaration for single variable is allowed
    y.test: 0.3

preprocessing: # data pre-processing [optional]
    # format: <variable name>.<feature name>:
    # - <model name>: <model 1>
    # - ...
    x.data:
    - vectorizer: TfIdfVectorizer
    - fillna: cool_feature.fillna # cunsom functions are allowed

features: # additional features [optional]
    # format: <variable name>:
    # - <feature>: <how to get this feature>
    # - ...
    x:
    # - is_holiday: [conditions syntax may be not presented] x.weekday == 6 or x.weekday == 7
    - is_stupid: cool_feature.get_stupid_values

params: # parameters for model selection [optional]
    # format: <model name>:
    # - <param name>: <value> or <values range>
    # - ...
    # values range should be used only for model selection
    vectorizer:
    - ngram_range: [1, 3]

model: # define models to apply on data
    # format: <result name>: <model name>
    # ...
    # previous results may be used on future steps
    x.knn: NearestNeighbors # add new feature to x variale
    end: LogisticRegression # final model

eval: # evaluate model results
    # format: - <metrics name>
    # - ...
    - accuracy
```

as a result following file should be generated:

```python
import pandas as pd
import numpy as np
import mylang  # import language engine itself
from sklearn.feature_selection import train_test_split
from sklearn.preprocessing.text import TfIdfVectorizer
from sklearn.model_selection import make_pipeline
# import
import my_module as cool_feature

# config
SEED = 42

x = pd.read_csv("path_to_x.csv")
y = pd.read_csv("path_to_y.csv")

x = x.loc[mylang.select.x, :]

x_train, x_test = train_test_split(random_state=SEED, train=0.7)
y_train, y_test = train_test_split(random_state=SEED, test=0.3)

x_proc = make_pipeline([
    model[**params] for model, params in zip(mylang.preprocessing.x, mylang.params.x.vectorizer)
])

x['is_stupid'] = cool_feature.get_stupid_values(x)
x['knn'] = NearestNeighbors(**mylang.params.x.knn)

model = LogisticRegression(**mylang.params.x.end)
model.fit(x_train, y_train)
y_hat = model.predict(x_test)

print(accuracy_score(y_test, y_hat))
```

Note: *this is only a draft of future syntax!*
