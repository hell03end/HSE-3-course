import logging
from copy import deepcopy
from typing import Callable, Iterable, NoReturn

from graphviz import Digraph

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d]"
           " %(message)s",
    datefmt="%H:%M:%S"
)

# borrowed from https://github.com/qntm/greenery/blob/master/greenery/lego.py
# Standard character classes
D = frozenset("0123456789")
W = D | frozenset("_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
S = frozenset(" \t\n\v\f\r")
SYMBOLS = frozenset("`~!@#%&=;:'\",<>/")
SPECIALS = frozenset("\|.()[]{}*+?-$^")
ALPHABET = W | S | SYMBOLS
ESCAPES = SPECIALS | frozenset("wrtsdfvbn")
ANY = ALPHABET | SPECIALS


class SpecialChars:
    """ Named special symbols. """
    OR = r"|"
    DOT = r"."
    ESCAPE = "\\"
    MULTIPLIERS = frozenset(r"*+?")
    SEQUENCIES = frozenset(r"([])")
    EPS = r""

    class Multipliers:
        STAR = r"*"
        PLUS = r"+"
        QM = r"?"

    class Sequencies:
        GROUP_START = r"("
        GROUP_END = r")"
        OR_SEQ_START = r"["
        OR_SEQ_END = r"]"


class Actions:
    OR = 0
    AND = 1
    STAR = 2
    PLUS = 3
    QM = 4
    MULTIPLIERS = {
        r"*": STAR,
        r"+": PLUS,
        r"?": QM
    }


class ParseError(Exception):
    """ Syntax error while parsing pattern. """

    def __init__(self,
                 msg: str="Syntax error while parsing pattern",
                 pos: int=None,
                 state: str=None,
                 char: str=None,
                 stack: list=None) -> NoReturn:
        """ :param stack :type typing.Iterable - any type of call stack """
        self.pos = pos
        self.state = state
        self.char = char
        self.stack = stack
        if self.state is not None:
            msg = f"{msg} [in state {self.state}]"
        if self.char is not None:
            msg = f"{msg} [on char '{self.char}']"
        if self.pos is not None:
            msg = f"{msg} [at {self.pos}]"
        if self.stack is not None:
            msg = f"{msg}\nstack: {self.stack}"
        super(ParseError, self).__init__(msg)


# [low priority] TODO: handle `{n,m}`, `a-z`,`$`, `^`.
def validate_regexp(pattern: str) -> NoReturn:
    """
        Validate regular expression pattern.

        MM-state machine is used. Throws ParseError exception.

        States
        ------
        A - wait for any char.
        E - wait for escaped char.
        G - wait for group end (`)`).
        O - wait for any char except or (`|`).
        S - wait for or sequence end (`]`).

        `(` is followed by A + G
        `[` is followed by A + S
        `|` is followed by O
        `\` is followed by E
        `*+?` are next to A or \ + E
    """
    states = {
        'A': lambda c: c in ANY,
        'E': lambda c: c in ESCAPES,
        'G': lambda c: c == r")",
        'O': lambda c: not c == r"|",
        'S': lambda c: c == r"]"
    }
    stack = []
    for pos, char in enumerate(pattern):
        logging.debug("Proceed\t(%d)\t`%s`\nstack:\t%s", pos, char, stack)

        if stack and stack[-1] == "E":
            state = stack.pop()
            if not states[state](char):
                raise ParseError("Invalid escape sequence",
                                 pos=pos, char=char, stack=stack)
            continue

        if char == SpecialChars.Sequencies.GROUP_START:
            if stack and stack[-1] in "AO":
                _ = stack.pop()
            if "S" in stack:
                continue
            stack.append("G")
            stack.append("A")
        elif char == SpecialChars.Sequencies.GROUP_END:
            if "S" in stack:
                if stack and stack[-1] in "AO":
                    _ = stack.pop()
                continue
            state = stack.pop() if stack else None
            if state != "G":
                raise ParseError("Invalid group detected",
                                 pos=pos, char=char, stack=stack)
        elif char == SpecialChars.Sequencies.OR_SEQ_START:
            if stack and stack[-1] in "AO":
                _ = stack.pop()
            if "S" in stack:
                continue
            stack.append("S")
            stack.append("A")
        elif char == SpecialChars.Sequencies.OR_SEQ_END:
            state = stack.pop() if stack else None
            if state != "S":
                raise ParseError("Invalid or sequence detected",
                                 pos=pos, char=char, stack=stack)
        elif char == SpecialChars.ESCAPE:
            # [reminder] TODO: if "E" in stack?
            if stack and stack[-1] in "AO":
                _ = stack.pop()
            stack.append("E")
        elif char == SpecialChars.OR:
            if stack and stack[-1] == "A":
                _ = stack.pop()
            stack.append("O")
        elif char in SpecialChars.MULTIPLIERS:
            try:
                # previous symbol should exists
                c = pattern[pos - 1]
                if c in SPECIALS - set(r")]"):
                    # symbol should be escaped
                    assert pattern[pos - 2] == "\\"
            except (IndexError, AssertionError) as err:
                raise ParseError("Invalid multiplier",
                                 pos=pos, char=char, stack=stack) from err
        elif stack:
            state = stack[-1]
            if state in "GS":
                continue
            state = stack.pop()
            if not states[state](char):
                raise ParseError(pos=pos, char=char, stack=stack)
    if stack:
        raise ParseError("Stack is not empty in the end of validation",
                         stack=stack)


class RecursiveDescent:
    """ Recursive descent regex parsing. """

    def __init__(self) -> NoReturn:
        self._connections = {}
        self._new_node = RecursiveDescent._get_new_node()

    @staticmethod
    def _get_new_node() -> Iterable:
        """ Generate unique name for node. """
        idx = -1
        while True:
            idx += 1
            node = f"q{idx}"
            logging.debug("Create new node: %s", node)
            yield node

    def _split_by_or(self, string: str, A: str=None, B: str=None) -> NoReturn:
        """ Splitting a string into "or" characters. """
        allowed_escapes = {SpecialChars.ESCAPE + c for c in
                           SpecialChars.SEQUENCIES | {SpecialChars.OR}}
        round_brackets_count = 0
        inside_square_brackets = False
        skip_next_char = False
        last_appended_idx = 0

        for idx, char in enumerate(string):
            logging.debug("Proceed\t(%d)\t`%s`", idx, char)

            if skip_next_char:
                skip_next_char = False
                continue

            if string[idx:idx + 2] in allowed_escapes:
                skip_next_char = True
                continue
            elif char == SpecialChars.Sequencies.GROUP_START:
                if not inside_square_brackets:
                    round_brackets_count += 1
            elif char == SpecialChars.Sequencies.GROUP_END:
                if not inside_square_brackets:
                    round_brackets_count -= 1
            elif char == SpecialChars.Sequencies.OR_SEQ_START:
                inside_square_brackets = True
            elif char == SpecialChars.Sequencies.OR_SEQ_END:
                inside_square_brackets = False
            elif char == SpecialChars.OR and \
                    not (inside_square_brackets or round_brackets_count) and \
                    string[last_appended_idx:idx + 1]:
                self._split_by_and(string[last_appended_idx:idx], A, B)
                last_appended_idx = idx + 1
        self._split_by_and(string[last_appended_idx:idx + 1], A, B)

    def _split_by_and(self, string: str, A: str=None, B: str=None) -> NoReturn:
        """ Splitting a string into it's basic component parts (tokens). """
        round_brackets_count = 0
        inside_square_brackets = False
        split = None
        idx = 0

        while idx < len(string) and not split:
            char = string[idx]
            logging.debug("Proceed\t(%d)\t`%s`", idx, char)

            if char == SpecialChars.Sequencies.GROUP_START:
                if not inside_square_brackets:
                    round_brackets_count += 1
            elif char == SpecialChars.Sequencies.GROUP_END:
                if not inside_square_brackets:
                    round_brackets_count -= 1
                    if not round_brackets_count:
                        if string[idx + 1:idx + 2] in SpecialChars.MULTIPLIERS:
                            idx += 1
                        split = [string[:idx + 1], string[idx + 1:]]
            elif char == SpecialChars.Sequencies.OR_SEQ_START:
                inside_square_brackets = True
            elif char == SpecialChars.Sequencies.OR_SEQ_END:
                inside_square_brackets = False
                if string[idx + 1:idx + 2] in SpecialChars.MULTIPLIERS:
                    idx += 1
                if not round_brackets_count:
                    split = [string[:idx + 1], string[idx + 1:]]
            elif inside_square_brackets or round_brackets_count:
                if char == SpecialChars.ESCAPE:
                    idx += 1
            elif char == SpecialChars.ESCAPE:
                idx += 1
                if string[idx + 1:idx + 2] in SpecialChars.MULTIPLIERS:
                    idx += 1
                split = [string[:idx + 1], string[idx + 1:]]
            elif char in ALPHABET | set(SpecialChars.DOT):
                if string[idx + 1:idx + 2] in SpecialChars.MULTIPLIERS:
                    idx += 1
                split = [string[:idx + 1], string[idx + 1:]]
            else:
                raise ParseError(pos=idx, char=char, stack=substrings)
            idx += 1
        logging.debug("Split: %s", split)
        if not split[-1]:
            self._connect_nodes(split[0], A, B)
        else:
            Q = next(self._new_node)
            self._connect_nodes(split[0], A, Q)
            self._split_by_and(split[-1], Q, B)

    @staticmethod
    def _escape_and_split(string: str) -> list:
        """ Escape each special char and split them. """
        tokens = []
        idx = 0
        while idx < len(string):
            char = string[idx]
            if char == SpecialChars.ESCAPE and \
                    string[idx + 1:idx + 2] in SPECIALS:
                tokens.append(string[idx:idx + 2])
                idx += 1
            elif char in SPECIALS:
                tokens.append(f"{SpecialChars.ESCAPE}{char}")
            else:
                tokens.append(char)
            idx += 1
        return tokens

    def connect(self,
                token: str,
                A: str,
                B: str,
                conns: dict=None) -> NoReturn:
        """ Create connection by 2 vertex in connection matrix. """
        connections = self._connections
        if conns:
            connections = conns
        if A not in connections:
            connections[A] = {}
        if B not in connections:
            connections[B] = {}
        if isinstance(token, str):
            token = [token]
        for t in token:
            t = t[-1] if len(t) > 1 else t
            if t not in connections[A]:
                connections[A][t] = []
            logging.debug("Create connection:\t{%s, %s: %s}", A, B, t)
            connections[A][t].append(B)

    def _connect_nodes(self, token: str, A: str=None, B: str=None) -> NoReturn:
        """ Create connection in connections matrix. """
        if token in ALPHABET or \
                (len(token) == 2 and token[0] == SpecialChars.ESCAPE):
            # a
            self.connect(token, A, B)
        elif (token[0] in ALPHABET and
                token[-1] == SpecialChars.Multipliers.PLUS) or \
                (token[0] == SpecialChars.ESCAPE and
                 token[-1] == SpecialChars.Multipliers.PLUS):
            # a+
            Q = next(self._new_node)
            self.connect(token[:-1], A, Q)
            self.connect(token[:-1], Q, Q)
            self.connect(SpecialChars.EPS, Q, B)
        elif (token[0] in ALPHABET and
                token[-1] == SpecialChars.Multipliers.STAR) or \
                (token[0] == SpecialChars.ESCAPE and
                 token[-1] == SpecialChars.Multipliers.STAR):
            # a*
            Q = next(self._new_node)
            self.connect(SpecialChars.EPS, A, Q)
            self.connect(token[:-1], Q, Q)
            self.connect(SpecialChars.EPS, Q, B)
        elif (token[0] in ALPHABET and
                token[-1] == SpecialChars.Multipliers.QM) or \
                (token[0] == SpecialChars.ESCAPE and
                 token[-1] == SpecialChars.Multipliers.QM):
            # a?
            self.connect(token[:-1], A, B)
            self.connect(SpecialChars.EPS, A, B)
        elif token[0] == SpecialChars.Sequencies.GROUP_START and \
                token[-1] == SpecialChars.Sequencies.GROUP_END:
            # ()
            self._split_by_or(token[1:-1], A, B)
        elif token[0] == SpecialChars.Sequencies.GROUP_START and \
                token[-1] == SpecialChars.Multipliers.PLUS:
            # ()+
            Q = next(self._new_node)
            self._split_by_or(token[1:-2], A, Q)
            self._split_by_or(token[1:-2], Q, Q)
            self.connect(SpecialChars.EPS, Q, B)
        elif token[0] == SpecialChars.Sequencies.GROUP_START and \
                token[-1] == SpecialChars.Multipliers.STAR:
            # ()*
            Q = next(self._new_node)
            self.connect(SpecialChars.EPS, A, Q)
            self._split_by_or(token[1:-2], Q, Q)
            self.connect(SpecialChars.EPS, Q, B)
        elif token[0] == SpecialChars.Sequencies.GROUP_START and \
                token[-1] == SpecialChars.Multipliers.QM:
            # ()?
            self._split_by_or(token[1:-2], A, B)
            self.connect(SpecialChars.EPS, A, B)
        elif token[0] == SpecialChars.Sequencies.OR_SEQ_START and \
                token[-1] == SpecialChars.Sequencies.OR_SEQ_END:
            # []
            tokens = self._escape_and_split(token[1:-1])
            self.connect(tokens, A, B)
        elif token[0] == SpecialChars.Sequencies.OR_SEQ_START and \
                token[-1] == SpecialChars.Multipliers.PLUS:
            # []+
            Q = next(self._new_node)
            tokens = self._escape_and_split(token[1: -2])
            self.connect(tokens, A, Q)
            self.connect(tokens, Q, Q)
            self.connect(SpecialChars.EPS, Q, B)
        elif token[0] == SpecialChars.Sequencies.OR_SEQ_START and \
                token[-1] == SpecialChars.Multipliers.STAR:
            # []*
            Q = next(self._new_node)
            tokens = self._escape_and_split(token[1: -2])
            self.connect(SpecialChars.EPS, A, Q)
            self.connect(tokens, Q, Q)
            self.connect(SpecialChars.EPS, Q, B)
        elif token[0] == SpecialChars.Sequencies.OR_SEQ_START and \
                token[-1] == SpecialChars.Multipliers.QM:
            # []?
            tokens = self._escape_and_split(token[1: -2])
            self.connect(tokens, A, B)
            self.connect(SpecialChars.EPS, A, B)

    def tokenize(self, pattern: str) -> dict:
        """
            Parse a regular expression using recursive descent.

            Split pattern to tokens and fill connection matrix:
            |     | c1 | c2 | c3 | c4 | c5 | EPS |
            |  I  |    |    |    |    |    |  E  |
            | ... |    |    |    |    |    |     |
            |  E  |    |    |    |    |    |     |
            where:
            * rows - states
            * columns - simple tokens
            * I - initial state
            * E - end state
        """
        logging.info("Proceed '%s'", pattern)
        self._split_by_or(pattern, "I", "E")
        return self._connections

    # [high priority] TODO: realization
    def to_fsm(self, connections: dict) -> dict:
        """ Remove eps states from connections. """

        end_node = "E"

        def add_conns(A: str, B: str, conns: dict=None) -> NoReturn:
            """ TODO """
            _connections = connections if not conns else conns
            for char, next_nodes in _connections[B].items():
                for nn in next_nodes:
                    if char not in _connections[A]:
                        _connections[A][char] = []
                    if char in _connections[A] and _connections[A][char] == nn:
                        continue
                    _connections[A][char].append(nn)

        def remove_eps(start_node: str, visited: set) -> tuple:
            """ TODO """
            stack = [start_node]
            for node in stack:
                while SpecialChars.EPS in connections[node] and \
                        connections[node][SpecialChars.EPS]:
                    next_node = connections[node][SpecialChars.EPS][0]
                    visited |= remove_eps(next_node, visited)
                    add_conns(node, next_node)
                    while next_node in connections[node][SpecialChars.EPS]:
                        connections[node][SpecialChars.EPS].remove(next_node)
                for char, next_nodes in connections[node].items():
                    for nn in next_nodes:
                        if char == SpecialChars.EPS:
                            del connections[node][SpecialChars.EPS]
                        elif not (nn in visited or nn in stack or nn == node):
                            stack.append(nn)
                visited.add(node)
            return visited

        def remove_commons(start_node: str) -> dict:
            """ TODO """
            new_conns = deepcopy(connections)
            conns_map = {}
            visited = set()
            stack = [start_node]
            for node in stack:
                new_nodes = []
                for char in connections[node].keys():
                    if len(connections[node][char]) <= 1:
                        continue

                    new_node = next(self._new_node)
                    new_nodes.append(new_node)
                    for next_node in connections[node][char]:
                        if next_node != end_node:
                            conns_map[next_node] = node
                        for ch, nns in connections[next_node].items():
                            for nn in nns:
                                while nn in conns_map:
                                    nn = conns_map[nn]
                                if new_node not in new_conns:
                                    new_conns[new_node] = {}
                                if ch not in new_conns[new_node]:
                                    new_conns[new_node][ch] = []
                                if nn not in new_conns[new_node][ch]:
                                    new_conns[new_node][ch].append(nn)

                    del new_conns[node][char]
                    new_conns[node][char] = [new_node]
                for new_node in new_nodes:
                    connections[new_node] = new_conns[new_node]

                for char, next_nodes in new_conns[node].items():
                    for nn in next_nodes:
                        if not (nn in visited or nn in stack or nn == node):
                            stack.append(nn)
                visited.add(node)
            return new_conns

        _ = remove_eps("I", set())
        # connections = remove_commons("I")

        return connections

    @staticmethod
    def simplify_connections(connections: dict) -> dict:
        """ Remove unused nodes. """
        visited = set()
        stack = ["I"]
        for node in stack:
            for char, new_nodes in connections[node].items():
                for new_node in new_nodes:
                    if new_node not in stack and new_node not in visited:
                        stack.append(new_node)
            visited.add(node)
        logging.debug("Remove nodes: %s", set(connections.keys()) - visited)
        for node in set(connections.keys()) - visited:
            del connections[node]
        return connections


class Regex(RecursiveDescent):
    """ Basic regex. """

    def __init__(self, pattern: str) -> NoReturn:
        super(Regex, self).__init__()
        validate_regexp(pattern)
        self._pattern = pattern
        self.tokenize(self._pattern)
        self._connections_ndfsm = self.simplify_connections(
            deepcopy(self._connections)
        )
        self._connections = self.to_fsm(deepcopy(self._connections_ndfsm))
        self._connections = self.simplify_connections(self._connections)

    # [log priority] TODO: realization
    def parse(text: str) -> list:
        """ Parse text with regular expression. """
        raise NotImplementedError

    def _graph(self, connections: dict) -> bytes:
        """ Creates graph image for internal FSM. """
        dot = Digraph(name="FSM", comment=self._pattern)
        dot.attr(rankdir='LR', size='8,5')
        # dot.attr('node', shape='doublecircle')
        for from_node, conns in connections.items():
            dot.node(from_node, from_node)
            for by_edge, to_nodes in conns.items():
                if not by_edge:
                    by_edge = "EPS"
                for to_node in to_nodes:
                    dot.edge(from_node, to_node, label=by_edge)
        return dot.pipe(format="png")

    def plot_graph(self, filename: str="FSM", ndfsm: bool=False) -> NoReturn:
        """ Saves internam FSM structure as graph. """
        connections = self._connections
        if ndfsm:
            connections = self._connections_ndfsm
        else:
            connections = self._connections
        with open(f"{filename}.png", 'bw') as fout:
            fout.write(self._graph(connections))


if __name__ == "__main__":
    """ This should be used for debug sessions only. """
    from pprint import pprint
    P = (
        # r"(a*)+",
        r"x?|(yz)+|t?",
        r"([a[|b])|((c|d)a)|(f(a|[1|2])d)",
        r"xy|xz|xyz|xtx|(a+x)*",
        r"xy|(xz)+|(xyz)*|(xt)?|(a+x)*",
        r"[a|b]|(c|d)a|b(a|[1|2])d",
        r"(a|[[b|c]\|)*|(b|c?)",
        r"[)x(]",
        r"([)x(])"
    )
    for i, p in enumerate(P):
        logging.info("=====%d=====", i)
        r = Regex(p)
        # pprint(r._connections)
        r.plot_graph(f"fsm-{i}", ndfsm=True)
        r.plot_graph(f"fsm-{i}-d")
