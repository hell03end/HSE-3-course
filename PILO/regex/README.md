Lab2
====
*for PILO course in HSE, Moscow.*

It's important to install dependencies before run the code:
```bash
# assume that pip3 and python3 is installed on your machine
pip3 install -r requirements.txt
```


Tests
-----
Use followed command to run tests:
```bash
pytest -v tests/test_regex.py
# There is a bug with tests runner: it supposed to start on `pytest -v` without args
```
