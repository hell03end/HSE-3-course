from typing import NoReturn

import pytest
from tests.fixtures import RegexFixture

from regex import ParseError, RecursiveDescent, validate_regexp
from utils import logging


def test_validate_regexp_positive() -> NoReturn:
    for idx, pattern in enumerate(RegexFixture.correct):
        logging.info("[Test] Proceed:\t%d\t%s", idx, pattern)
        assert validate_regexp(pattern) is None


def test_validate_regexp_negative() -> NoReturn:
    for idx, pattern in enumerate(RegexFixture.incorrect):
        logging.info("[Test] Proceed:\t%d\t%s", idx, pattern)
        with pytest.raises(ParseError) as err_info:
            validate_regexp(pattern)
        assert err_info


class TestRecursiveDescent:
    def test__split_by_or(self) -> NoReturn:
        for idx, (pattern, output) in enumerate(
                zip(RegexFixture.f1_tests, RegexFixture.f1_tests_outputs)):
            logging.info("[Test] Proceed:\t%d\t%s", idx, pattern)
            assert list(RecursiveDescent._split_by_or(pattern)) == output

    def test__split_by_and(self) -> NoReturn:
        for idx, (pattern, output) in enumerate(
                zip(RegexFixture.f2_tests, RegexFixture.f2_outputs)):
            logging.info("[Test] Proceed:\t%d\t%s", idx, pattern)
            assert list(RecursiveDescent._split_by_and(pattern)) == output
