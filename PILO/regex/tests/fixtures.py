from regex import Actions


class RegexFixture(object):
    # 'sa' stands for "sub alphabet"
    correct = (
        r"",
        r"hello, world!",
        r"(ab)+",
        r"a-z",
        r"[a-zA-Z]",
        r"[abc]",
        r"(a|b)",
        r"(a|b|c)",
        r"a+",
        r"a?",
        r"a*",
        r".",
        r"(a)",
        r"(a(a(a|b)))",
        r"(((a)))",
        r"[\+\-\\\.\*\?\[\]]",
        r"[^ab]",
        r"a{2}",
        r"a{0,1}",
        r"a{2,}",
        # from sa
        r"abc",
        r"a[bc]",
        r"a(bc)",
        r"(ab)",
        r"a|b|c",
        r"a b c",
        r"([ab])|cd",
        r"[ab][cd]",
        r"[ab]|[cd]",
        r"[ab]|(cd)",
        r"m[ab]c|d[ef]g",
        r"[b]|[c21][1]|(ca|sd)",
        r"[a|b)(]",
        r"[[\]]",
        "OTHER_TESTS",
        r"c((a+(b|c)*)+|a+)|b|([12]|b|c)+(def)+|[67](hb)|[qwe][op]|(pop)(ul)",
        r"c((a+(bc)*)+|a+)|b",
        r"(s[um]|k+)|(do)+|(kt)(m)",
        r"[as|bd]|(mb|p|s)",
        r"[a|b]|(c|d)a|b(a|[1|2])d",
        r"[)x(]",
        r"([)x(])",
        r"\(\( s (abc[d]) | def \)\)|a",
        r"[))q\[we((]|(abc)|ad",
        r"[[[[[ ab|c ]+|de",
        r"[ab(de)f)fe([q(\]]|(b+)|[c]+",
        # from f1
        r"(\(a\))|\(x\)",
        r"(a|[[b|c]\|)+|(b|c+)",
        r"c((a+(b|c)*)+|a+)|b|([12]|b|c)+(def)+|[67](hb)|[qwe][op]|(pop)(ul)",
        r"(s[um]|k+)|(do)+|(kt)(m)",
        r"[as|bd]|(mb|p|s)",
        r"[a|b]|(c|d)a|b(a|[1|2])d",
        r"[)x(]",
        r"([)x(])",
        r"\(\( s (abc[d]) | def \)\)|a",
        r"[))q\[we((]|(abc)|ad",
        r"[[[[[ ab|c ]+|de",
        r"[ab(de)f)fe([q(\]]|(b+)|[c]+",
        r"((a|s)|(b|[s\|n[\]|d]+)|na)*|[12]+|(12[)(()\)\[\]])+"
    )

    incorrect = (
        # r"[a-]",
        # r"[^]",
        # r"a{-1}",
        # r"{1}",
        # r"a{,}",
        # r"a{1,1,}",
        # from sa
        r"ab(",
        r"cd)",
        r"e[f",
        r"g]",
        r"(12))",
        r"[[12]]",
        r"ab|",
        r"(())",
        r"[([(x)])]",
        r"[]",
        r"()",
        r"|",
        r"(|)",
        r"(",
        r")",
        r"[",
        r"]",
        r"(|",
        r"|)",
        r"+",
        r"?",
        r"*",
        r"(a(a((a)))",
        r"([a(a(]a)))",
        r"[\]"
    )

    f1_tests = (
        r"a",  # to check no "index out of range" exceptions occurs
        r"(\(a\))|\(x\)",
        r"(a|[[b|c]\|)+|(b|c+)",
        r"c((a+(b|c)*)+|a+)|b|([12]|b|c)+(def)+|[67](hb)|[qwe][op]|(pop)(ul)",
        r"(s[um]|k+)|(do)+|(kt)(m)",
        r"[as|bd]|(mb|p|s)",
        r"[a|b]|(c|d)a|b(a|[1|2])d",
        r"[)x(]",
        r"([)x(])",
        r"\(\( s (abc[d]) | def \)\)|a",
        r"[))q\[we((]|(abc)|ad",
        r"[[[[[ ab|c ]+|de",
        r"[ab(de)f)fe([q(\]]|(b+)|[c]+",
        r"((a|s)|(b|[s\|n[\]|d]+)|na)*|[12]+|(12[)(()\)\[\]])+"
    )

    f1_tests_outputs = (
        ['a'],
        ['(\\(a\\))', '\\(x\\)'],
        ['(a|[[b|c]\\|)+', '(b|c+)'],
        ['c((a+(b|c)*)+|a+)', 'b', '([12]|b|c)+(def)+', '[67](hb)',
         '[qwe][op]', '(pop)(ul)'],
        ['(s[um]|k+)', '(do)+', '(kt)(m)'],
        ['[as|bd]', '(mb|p|s)'],
        ['[a|b]', '(c|d)a', 'b(a|[1|2])d'],
        ['[)x(]'],
        ['([)x(])'],
        ['\\(\\( s (abc[d]) ', ' def \\)\\)', 'a'],
        ['[))q\\[we((]', '(abc)', 'ad'],
        ['[[[[[ ab|c ]+', 'de'],
        ['[ab(de)f)fe([q(\\]]', '(b+)', '[c]+'],
        ['((a|s)|(b|[s\\|n[\\]|d]+)|na)*', '[12]+', '(12[)(()\\)\\[\\]])+']
    )

    f2_tests = (
        r"a",
        r"ab*",
        r"ab*(acd)",
        r"ab*(acd)*",
        r"ab*((acd))",
        r"ab*((acd))*",
        r"(ab)*((acd))*",
        "NEW TESTS",
        r"0. (\(a\))\(x\)",
        r"1. x\|x",
        r"2. \|+",
        r"3. [)x(]",
        r"4. ([)x(])*",
        r"5. \(* or \(+",
        r"6. [[[ab|c]+de",
        r"7. [\]]",
        r"8. \?"
    )

    f2_outputs = (
        ['a'],
        ['a', 'b*'],
        ['a', 'b*', '(acd)'],
        ['a', 'b*', '(acd)*'],
        ['a', 'b*', '((acd))'],
        ['a', 'b*', '((acd))*'],
        ['(ab)*', '((acd))*'],
        ['N', 'E', 'W', ' ', 'T', 'E', 'S', 'T', 'S'],
        ['0', '.', ' ', '(\\(a\\))', '\\(', 'x', '\\)'],
        ['1', '.', ' ', 'x', '\\|', 'x'],
        ['2', '.', ' ', '\\|+'],
        ['3', '.', ' ', '[)x(]'],
        ['4', '.', ' ', '([)x(])*'],
        ['5', '.', ' ', '\\(*', ' ', 'o', 'r', ' ', '\\(+'],
        ['6', '.', ' ', '[[[ab|c]+', 'd', 'e'],
        ['7', '.', ' ', '[\\]]'],
        ['8', '.', ' ', '\?']
    )

    tokens_tests = (
        r"a",
        r"(\(a\))|\(x\)",
        r"[as|bd]|(mb|p|s)",
        r"[a|b]|(c|d)a|b(a|[1|2])d",
        r"[)x(]",
        r"([)x(])",
        r"(a|[[b|c]\|)*|(b|c?)",
        r"(s[um]|k*)|(do)?|(kt)(m)",
        r"[[[[[ ab|c ]*|de",
        r"[))q\[we((]|(abc)|ad",
        r"\(\( s (abc[d]) | def \)\)|a",
        r"[ab(de)f)fe([q(\]]|(b+)|[c]?",
        r"((a|s)|(b|[s\|n[\]|d]?)|na)*|[12]*|(12[)(()\)\[\]])*"
    )

    # [medium priority] TODO: finish tokens for all testcases
    tokens = (
        [
            [
                'a',
                Actions.AND
            ],
            Actions.OR
        ],  # r"a"
        [
            [
                [
                    [
                        '\\(',
                        'a',
                        '\\)',
                        Actions.AND
                    ],
                    Actions.OR
                ],
                Actions.AND
            ],
            [
                '\\(',
                'x',
                '\\)',
                Actions.AND
            ],
            Actions.OR
        ],  # r"(\(a\))|\(x\)"
        [
            [
                [
                    'a',
                    's',
                    '|',
                    'b',
                    'd',
                    Actions.OR
                ],
                Actions.AND
            ],
            [
                [
                    [
                        'm',
                        'b',
                        Actions.AND
                    ],
                    [
                        'p',
                        Actions.AND
                    ],
                    [
                        's',
                        Actions.AND
                    ],
                    Actions.OR
                ],
                Actions.AND
            ],
            Actions.OR
        ],  # r"[as|bd]|(mb|p|s)"
        [
            [
                [
                    'a',
                    '|',
                    'b',
                    Actions.OR
                ],
                Actions.AND
            ],
            [
                [
                    [
                        'c',
                        Actions.AND
                    ],
                    [
                        'd',
                        Actions.AND
                    ],
                    Actions.OR
                ],
                'a',
                Actions.AND
            ],
            [
                'b',
                [
                    [
                        'a',
                        Actions.AND
                    ],
                    [
                        [
                            '1',
                            '|',
                            '2',
                            Actions.OR
                        ],
                        Actions.AND
                    ],
                    Actions.OR
                ],
                'd',
                Actions.AND
            ],
            Actions.OR
        ],  # r"[a|b]|(c|d)a|b(a|[1|2])d"
        [
            [
                [
                    ')',
                    'x',
                    '(',
                    Actions.OR
                ],
                Actions.AND
            ],
            Actions.OR
        ],  # r"[)x(]"
        [
            [
                [
                    [
                        [
                            ')',
                            'x',
                            '(',
                            Actions.OR
                        ],
                        Actions.AND
                    ],
                    Actions.OR,
                ],
                Actions.AND
            ],
            Actions.OR
        ],  # r"([)x(])"
        [
            [
                [
                    [
                        [
                            'a',
                            Actions.AND
                        ],
                        [
                            [
                                '[',
                                'b',
                                '|',
                                'c',
                                Actions.OR
                            ],
                            '\\|',
                            Actions.AND
                        ],
                        Actions.OR
                    ],
                    Actions.STAR
                ],
                Actions.AND
            ],
            [
                [
                    [
                        'b',
                        Actions.AND
                    ],
                    [
                        [
                            'c',
                            Actions.QM
                        ],
                        Actions.AND
                    ],
                    Actions.OR
                ],
                Actions.AND
            ],
            Actions.OR
        ],  # r"(a|[[b|c]\|)*|(b|c?)"
        [
            [
                [
                    [
                        's',
                        [
                            'u',
                            'm',
                            Actions.OR
                        ],
                        Actions.AND
                    ],
                    [
                        [
                            'k',
                            Actions.STAR
                        ],
                        Actions.AND
                    ],
                    Actions.OR
                ],
                Actions.AND
            ],
            [
                [
                    [
                        [
                            'd',
                            'o',
                            Actions.AND
                        ],
                        Actions.OR
                    ],
                    Actions.QM
                ],
                Actions.AND
            ],
            [
                [
                    [
                        'k',
                        't',
                        Actions.AND
                    ],
                    Actions.OR
                ],
                [
                    [
                        'm',
                        Actions.AND
                    ],
                    Actions.OR
                ],
                Actions.AND
            ],
            Actions.OR
        ],  # r"(s[um]|k*)|(do)?|(kt)(m)"
        [],  # r"[[[[[ ab|c ]+|de"
        [],  # r"[))q\[we((]|(abc)|ad"
        [],  # r"\(\( s (abc[d]) | def \)\)|a"
        [],  # r"[ab(de)f)fe([q(\]]|(b+)|[c]+"
        []  # r"((a|s)|(b|[s\|n[\]|d]+)|na)*|[12]+|(12[)(()\)\[\]])+"
    )

    # [medium priority] TODO: finish connections for all testcases
    connections = (
        {'E': {}, 'I': {'a': 'E'}},  # r"a"
        {
            hex(hash('0:I:\\(')): {
                'a': hex(hash('1:0:I:\\(:a')),
                'x': hex(hash('1:0:I:\\(:x'))
            },
            hex(hash('1:0:I:\\(:a')): {')': 'E'},
            hex(hash('1:0:I:\\(:x')): {')': 'E'},
            'E': {},
            'I': {'(': hex(hash('0:I:\\('))}
        },  # r"(\(a\))|\(x\)"
        {
            hex(hash('0:I:m')): {'b': 'E'},
            'E': {},
            'I': {
                'a': 'E',
                'b': 'E',
                'd': 'E',
                'm': hex(hash('0:I:m')),
                'p': 'E',
                's': 'E',
                '|': 'E'
            }
        },  # r"[as|bd]|(mb|p|s)"
        {
            hex(hash('0:I:(c|d)')): {'a': 'E'},
            hex(hash('0:I:b')): {
                '1': hex(hash('1:0:I:b:(a|[1|2])')),
                '2': hex(hash('1:0:I:b:(a|[1|2])')),
                'a': hex(hash('1:0:I:b:(a|[1|2])')),
                '|': hex(hash('1:0:I:b:(a|[1|2])'))
            },
            hex(hash('1:0:I:b:(a|[1|2])')): {'d': 'E'},
            'E': {},
            'I': {
                'a': 'E',
                'b': 'E',
                'c': hex(hash('0:I:(c|d)')),
                'd': hex(hash('0:I:(c|d)')),
                '|': 'E'
            }
        },  # r"[a|b]|(c|d)a|b(a|[1|2])d"
        {'E': {}, 'I': {'(': 'E', ')': 'E', 'x': 'E'}},  # r"[)x(]"
        {'E': {}, 'I': {'(': 'E', ')': 'E', 'x': 'E'}},  # r"([)x(])"
        {
            hex(hash('0:I:*:[[b|c]')): {'|': hex(hash('I:*'))},
            'E': {},
            'I': {'': hex(hash('I:*')), 'b': 'E', 'c': 'E'},
            hex(hash('I:*')): {
                '': 'E',
                '[': hex(hash('0:I:*:[[b|c]')),
                'a': hex(hash('I:*')),
                'b': hex(hash('0:I:*:[[b|c]')),
                'c': hex(hash('0:I:*:[[b|c]')),
                '|': hex(hash('0:I:*:[[b|c]'))
            }
        },  # r"(a|[[b|c]\|)*|(b|c?)"
        {
            hex(hash('0:I:(kt)')): {'m': 'E'},
            hex(hash('0:I:d')): {'o': 'E'},
            hex(hash('0:I:k')): {'t': hex(hash('0:I:(kt)'))},
            hex(hash('0:I:s')): {'m': 'E', 'u': 'E'},
            'E': {},
            'I': {
                '': hex(hash('I:*')),
                'd': hex(hash('0:I:d')),
                'k': hex(hash('0:I:k')),
                's': hex(hash('0:I:s'))
            },
            hex(hash('I:*')): {'': 'E', 'k': hex(hash('I:*'))}
        },  # r"(s[um]|k*)|(do)?|(kt)(m)"
        {},  # r"[[[[[ ab|c ]+|de"
        {},  # r"[))q\[we((]|(abc)|ad"
        {},  # r"\(\( s (abc[d]) | def \)\)|a"
        {},  # r"[ab(de)f)fe([q(\]]|(b+)|[c]+"
        {}  # r"((a|s)|(b|[s\|n[\]|d]+)|na)*|[12]+|(12[)(()\)\[\]])+"
    )
