"""
    Написать программу, осуществляющую проверку входных цепочек с
    использованием примитивного МП - автомата.

    { 1^n 0^m | n > m >= 0 }
"""

import logging

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d]"
           " %(message)s",
    datefmt="%H:%M:%S"
)

ALPHABET = {"0", "1"}


def validate_pattern(pattern: str) -> bool or None:
    """
        Check pattern is valid against hardcoded expression:
        { 1^n 0^m | n > m >= 0 }

        MM-state machine is used.

        States
        ------
        A - wait for 1.
        B - wait for 0.

        `0` before `1` cause an error
        `1` is followed by A if no B in stack, else cause error
        `0` is followed by removing A (+ B if B in stack) and add B
    """
    logging.debug("Proceed pattern:\t'%s'", pattern)
    stack = ["I"]
    for char in pattern:
        logging.debug("Proceed:\t%c\t%s", char, stack)
        if char not in ALPHABET:
            logging.warning("Found invalid char\t`%c`", char)
            return
        if char == "1" and stack[-1] == "I":
            _ = stack.pop()
            stack.append("E")
            stack.append("A")
            stack.append("B")
        elif char == "1" and stack[-1] == "B":
            # replace B with AB
            _ = stack.pop()
            stack.append("A")
            stack.append("B")
        elif char == "0" and stack[-1] == "B":
            # remove AB
            _ = stack.pop()
            _ = stack.pop()
        elif char == "0" and stack[-1] == "A":
            # remove A
            _ = stack.pop()
        else:
            logging.debug("Invalid state!")
            return False
    return stack[-1] == "A"


if __name__ == "__main__":
    logging.debug("Start of \"%s\".", __file__)

    incorrect = (
        "",
        "0",
        "00",
        "000",
        "0001",
        "1",
        "11",
        "111",
        "011",
        "001",
        "0011",
        "00110",
        "01011",
        "111000",
        "10101",
        "01010",
        "000011111",
        "000011111101",
        "00111",
        "0000111111",
        "000000111111111"
    )

    correct = (
        "110",
        "1110",
        "11100",
        "11110",
        "111100",
        "1111000",
        "111110",
        "1111100",
        "11111000"
    )

    for idx, pattern in enumerate(incorrect):
        logging.info("incorr:\t%d\t%s", idx, pattern)
        assert not validate_pattern(pattern)

    for idx, pattern in enumerate(correct):
        logging.info("corr:\t%d\t%s", idx, pattern)
        assert validate_pattern(pattern)

    # while True:
    #     pattern = input(f"Enter pattern from {ALPHABET}:\t").strip()
    #     result = validate_pattern(pattern)
    #     if result is None:
    #         print("EXIT")
    #         break
    #     else:
    #         print("VALID" if result else "INVALID")

    logging.debug("End of \"%s\".", __file__)
