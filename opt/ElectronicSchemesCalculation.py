import numpy as np
import scipy as sc
import pandas as pd


def fit_gauss(A, b):
    slau = np.hstack((A, b))
    triangle_u = gauss_forward(slau)
    x = gauss_backward(triangle_u)
    return x


def swap_rows(matrix, row1, col, is_swapped):
    for row2 in range(row1+1, matrix.shape[0], 1):
        if matrix[row2, col] != 0:
            temp = matrix[row1].copy()
            matrix[row1] = matrix[row2]
            matrix[row2] = temp
            is_swapped = True
            break
    return matrix, is_swapped


def gauss_forward(matrix):
    iterations = int((1+matrix.shape[0])/2*matrix.shape[0])
    k = 0
    j = 1
    while j < matrix.shape[1]-1+k:
        for i in range(j-k, matrix.shape[0]):
            # print(matrix)
            # print('='*50)
            is_swapped = False
            if matrix[j-1-k, j-1] == 0:
                matrix, is_swapped = swap_rows(matrix, j-1, j-1, is_swapped)
                if not is_swapped:
                    #print("i,j =",i,j)
                    #print("not swap")
                    k += 1
                    continue
            matrix[i, :] = matrix[i, :] - matrix[j-1-k, :] * \
                (matrix[i, j-1]/matrix[j-1-k, j-1])
            # print(matrix)
            # print('-'*100)
        #print("lim", matrix.shape[1]-1+k)
        j += 1
    return matrix


def gauss_backward(matrix, low_triangle=False):
    values = matrix[:, matrix.shape[1]-1].copy()
    x = np.zeros((matrix.shape[1]-1,))
    if low_triangle:
        for i in range(matrix.shape[0]):
            values[i] = values[i] - (x*matrix[i, :matrix.shape[1]-1]).sum()
            x[i] = values[i]/matrix[i, i]
    else:
        for i in range(matrix.shape[0]-1, -1, -1):
            values[i] = values[i] - (x*matrix[i, :matrix.shape[1]-1]).sum()
            # print(values)
            x[i] = values[i]/matrix[i, i]
            # print(answers)
            # print("-"*100)
    return x.reshape(-1, 1)


def pred(r, connection_matrix, eds_matrix, get_A=False, get_b=False):
    g = r_to_g(r)
    A, b = define_slau(g, eds_matrix, connection_matrix)
    u_pred = fit_gauss(A, -b)
    if get_A:
        if get_b:
            return u_pred, A, b
        return u_pred, A
    else:
        if get_b:
            return u_pred, b
    return u_pred


def loss_calc(u_pred, u_target):
    return (u_pred - u_target)**2


def define_slau(g, eds_matrix, connection_matrix, get_slau=0):
    conductions_matrix = np.zeros((r.shape[0], r.shape[0]), dtype=np.float32)
    conductions_matrix[np.diag_indices_from(conductions_matrix)] = g

    kirch_matrix = np.matmul(connection_matrix, conductions_matrix)
    kirch_matrix = np.matmul(kirch_matrix, connection_matrix.transpose())

    sources = np.matmul(conductions_matrix, eds_matrix)
    sources = np.matmul(-connection_matrix, sources)

    slau = np.hstack((kirch_matrix, sources))

    if get_slau == 0:
        return kirch_matrix, sources
    elif get_slau == 1:
        return slau
    elif get_slau == 2:
        return kirch_matrix, sources, slau


def g_to_r(r):
    return 1/r


def r_to_g(g):
    return 1/g


def get_g_ind_deriv(A, u_pred, connection_matrix, g, g_ind):
    n = len(connection_matrix)
    g_matrix = np.zeros((n, n))
    i = np.argwhere(connection_matrix[:, g_ind] != 0).flatten()
    if len(i) == 1:
        print("ERROR: this resistor can't be variable")
    g_matrix[i[0], i[0]] = -g[g_ind]**2
    g_matrix[i[0], i[1]] = g[g_ind]**2
    g_matrix[i[1], i[0]] = g[g_ind]**2
    g_matrix[i[1], i[1]] = -g[g_ind]**2
    b = np.matmul(g_matrix, u_pred)
    return fit_gauss(A, -b)


def grad_i_calc(A, u_target, u_pred, connection_matrix, g, variable_g_inds, u_deriv):
    grad = np.zeros((len(g)),)
    for var in variable_g_inds:
        deriv = get_g_ind_deriv(A, u_pred, connection_matrix, g, var)[u_deriv]
        grad[var] = 2*(u_pred[u_deriv] - u_target)*deriv
    return grad


def gradient_descent(u_target, u_deriv, r, variable_g_inds, connection_matrix, eds_matrix, learning_rate, iterations, eps=0.001):
    loss_val = list()
    step = np.zeros((len(r),))
    for i in range(iterations):
        g = r_to_g(r)
        u_pred, A = pred(r, connection_matrix, eds_matrix, get_A=True)
        loss_tmp = loss_calc(u_target, u_pred[u_deriv])
        loss_val.append(loss_tmp)
        grad = grad_i_calc(A, u_target, u_pred,
                           connection_matrix, g, variable_g_inds, u_deriv)
        step = learning_rate * grad
        for j in range(len(step)):
            if r[j] - step[j] > 0:
                r[j] -= step[j]
        if eps >= loss_val[i]:
            return np.asarray(loss_val), r
        if loss_val[i] > loss_val[i-1]:
            learning_rate = learning_rate/2
        elif loss_val[i-1] - loss_val[i] < 0.01:
            learning_rate = learning_rate/2
    return np.asarray(loss_val), r

# сопряженных направлений нулевого порядка


def conj_directions(u_target, u_deriv, r, variable_g_inds, connection_matrix, eds_matrix, eps=0.5):
    loss_val = list()

    n = len(variable_g_inds)
    p = np.zeros((n, n+1))
    p_m = np.zeros((n, n+1))
    diag = np.diag_indices_from(p[:, 1:])
    p[:, 1:][diag] = 1
    p[:, 0] = p[:, len(variable_g_inds)]

    learning_rate = 8
    iterations = 500
    k = 0
    r_y = [r[variable_g_inds].copy()]
    is_stop = False
    while (not is_stop):
        u_pred = pred(r, connection_matrix, eds_matrix)
        loss_tmp = loss_calc(u_target, u_pred[u_deriv])
        loss_val. append(loss_tmp)
        for i in range(0, n+1):
            r_tmp = r.copy()
            r_tmp[variable_g_inds] = r_y[i].copy()
            new_r = onedim_optim(u_target, u_deriv, r_tmp, variable_g_inds, connection_matrix, eds_matrix,
                                 p[:, i])[variable_g_inds]
            r_y.append(new_r)
            if i == n-1:
                if all(r_y[i] == r_y[0]):
                    return np.asarray(loss_val), r_y[i]
            elif i == n:
                if all(r_y[i+1] == r_y[1]):
                    return np.asarray(loss_val), r_y[i]
                else:
                    if np.linalg.norm(r_y[i+1] - r_y[0]) < eps:
                        return np.asarray(loss_val), r_y[i+1]
                    else:
                        p_m[:, 0] = p_m[:, n] = r_y[i] - r_y[1]
                        for j in range(1, n):
                            p_m[:, j] = p[:, j+1]
                        if np.linalg.matrix_rank(p_m[:, 1:]) == n:
                            p = p_m.copy()
                        r[variable_g_inds] = r_y[i].copy()
                        r_y = [r[variable_g_inds].copy()]
                        k = k+1
                        if k == 1000:
                            is_stop = True
    return np.asarray(loss_val), r_y[-1]


# покоординатный спуск
def CoordDescent(u_target, u_deriv, r, variable_g_inds, connection_matrix, eds_matrix, learning_rate, iterations, eps=0.1):
    loss_val = list()
    step_optim = learning_rate
    u_pred = pred(r, connection_matrix, eds_matrix)
    loss_tmp = loss_calc(u_target, u_pred[u_deriv])
    loss_val.append(loss_tmp[0])
    for i in range(iterations):
        is_steped = False
        for j in variable_g_inds:

            # choose direction
            # ---
            save_r = r[j]

            r[j] = r[j] + step_optim
            u_pred = pred(r, connection_matrix, eds_matrix)
            loss_p = loss_calc(u_target, u_pred[u_deriv])

            r[j] = save_r
            r[j] = r[j] - step_optim
            u_pred = pred(r, connection_matrix, eds_matrix)
            loss_m = loss_calc(u_target, u_pred[u_deriv])

            r[j] = save_r
            if loss_p < loss_m:
                step_ax = step_optim
            else:
                step_ax = -step_optim
            # ---

            # coord descent
            loss_tmp = 0
            while(loss_tmp < loss_val[-1]):
                save_r = r[j]
                if r[j] + step_ax > 0:
                    r[j] = r[j] + step_ax
                else:
                    break
                u_pred = pred(r, connection_matrix, eds_matrix)
                loss_tmp = loss_calc(u_target, u_pred[u_deriv])
                if loss_tmp < loss_val[-1]:
                    is_steped = True
                    loss_val.append(loss_tmp)
                else:
                    r[j] = save_r

        # step reduction
        if not is_steped:
            step_optim = step_optim/2
        # converged
        if step_optim < eps:
            np.asarray(loss_val), r
    return np.asarray(loss_val), r


def Hooke_Jeeves(u_target, u_deriv, r, variable_g_inds, connection_matrix, eds_matrix, learning_rate, iterations, eps=1, lmbd=2):
    loss_val = list()
    step_optim = np.zeros(len(r),) + learning_rate
    u_pred = pred(r, connection_matrix, eds_matrix)
    loss_tmp = loss_calc(u_target, u_pred[u_deriv])
    loss_val.append(loss_tmp[0])
    for i in range(iterations):
        root_r = r.copy()
        step_stop = [False]*len(variable_g_inds)
        for k, j in enumerate(variable_g_inds):
            is_step = False
            step_ax = step_optim.copy()
            while (not is_step):
                save_r = r[j]

                if r[j] + step_ax[j] > 0:
                    r[j] = r[j] + step_ax[j]
                    u_pred = pred(r, connection_matrix, eds_matrix)
                    loss_p = loss_calc(u_target, u_pred[u_deriv])
                else:
                    loss_p = 10000000
                r[j] = save_r

                if r[j] - step_ax[j] > 0:
                    r[j] = r[j] - step_ax[j]
                    u_pred = pred(r, connection_matrix, eds_matrix)
                    loss_m = loss_calc(u_target, u_pred[u_deriv])
                else:
                    loss_m = 10000000
                r[j] = save_r

                # choose direct
                m = np.array([loss_m, loss_p, loss_val[-1]]).argmin()
                if m == 0:
                    if r[j] - step_ax[j] > 0:
                        r[j] = r[j] - step_ax[j]
                        is_step = True
                elif m == 1:
                    if r[j] + step_ax[j] > 0:
                        r[j] = r[j] + step_ax[j]
                        is_step = True
                else:
                    new_step = step_ax[j]/2
                    if new_step > eps:
                        step_ax[j] = step_ax[j]/2
                    else:
                        is_step = True
                        step_stop[k] = True
        if all(step_stop):
            u_pred = pred(r, connection_matrix, eds_matrix)
            loss_tmp = loss_calc(u_target, u_pred[u_deriv])
            loss_val.append(loss_tmp)
            return np.asarray(loss_val), r
        is_founded = True
        while (is_founded):
            is_founded = False
            if all(root_r + lmbd*(r - root_r) > 0):
                root2_r = root_r + lmbd*(r - root_r)
                u_pred = pred(root2_r, connection_matrix, eds_matrix)
                loss_root2 = loss_calc(u_target, u_pred[u_deriv])
                r4 = root2_r.copy()
                for j in variable_g_inds:
                    save_r = r4[j]

                    if r4[j] + step_ax[j] > 0:
                        r4[j] = r4[j] + step_ax[j]
                        u_pred = pred(r4, connection_matrix, eds_matrix)
                        loss_p = loss_calc(u_target, u_pred[u_deriv])
                    else:
                        loss_p = 10000000
                    r4[j] = save_r

                    if r4[j] - step_ax[j] > 0:
                        r4[j] = r4[j] - step_ax[j]
                        u_pred = pred(r4, connection_matrix, eds_matrix)
                        loss_m = loss_calc(u_target, u_pred[u_deriv])
                    else:
                        loss_m = 10000000
                    r4[j] = save_r
                    # choose direct
                    m = np.array([loss_m, loss_p, loss_root2]).argmin()
                    if m == 0:
                        if r4[j] - step_ax[j] > 0:
                            r4[j] = r4[j] - step_ax[j]
                    elif m == 1:
                        if r4[j] + step_ax[j] > 0:
                            r4[j] = r4[j] + step_ax[j]

                u_pred = pred(r4, connection_matrix, eds_matrix)
                loss_r4 = loss_calc(u_target, u_pred[u_deriv])
                if (loss_r4 < loss_root2):
                    is_founded = True
                    root_r = r.copy()
                    r = r4.copy()
        u_pred = pred(r, connection_matrix, eds_matrix)
        loss_tmp = loss_calc(u_target, u_pred[u_deriv])
        loss_val. append(loss_tmp)
    return np.asarray(loss_val), r


# одномерной оптимизации
def onedim_optim(u_target, u_deriv, r, variable_g_inds, connection_matrix, eds_matrix, p, eps=0.1, eps_loss=1):
    step_optim = p
    while(np.linalg.norm(step_optim) > eps):
        is_inner = False
        # choose direction
        save_r = r[variable_g_inds]

        if all(r[variable_g_inds] + step_optim > 0):
            r[variable_g_inds] = r[variable_g_inds] + step_optim
            u_pred = pred(r, connection_matrix, eds_matrix)
            loss_p = loss_calc(u_target, u_pred[u_deriv])
        else:
            loss_p = 100000
        r[variable_g_inds] = save_r

        if all(r[variable_g_inds] - step_optim > 0):
            r[variable_g_inds] = r[variable_g_inds] - step_optim
            u_pred = pred(r, connection_matrix, eds_matrix)
            loss_m = loss_calc(u_target, u_pred[u_deriv])
        else:
            loss_m = 100000
        r[variable_g_inds] = save_r

        if loss_p < loss_m:
            step_ax = step_optim
            is_inner = True
        elif loss_p > loss_m:
            step_ax = -step_optim
            is_inner = True
        else:
            step_optim = step_optim/2
        # coord descent
        loss_prev = 100000000
        while(is_inner):
            save_r = r[variable_g_inds]
            if all(r[variable_g_inds] + step_ax > 0):
                r[variable_g_inds] = r[variable_g_inds] + step_ax
                u_pred = pred(r, connection_matrix, eds_matrix)
                loss_curr = loss_calc(u_target, u_pred[u_deriv])
                if loss_curr < loss_prev:
                    loss_prev = loss_curr
                if loss_prev - loss_curr < eps_loss:
                    return r
                else:
                    step_optim = step_optim/2
                    r[variable_g_inds] = save_r
                    break
            else:
                step_optim = step_optim/2
                break
    return r


# сопряженных координат
def conj_grad(u_target, u_deriv, r, variable_g_inds, connection_matrix, eds_matrix, iterations, eps_grad=0.1, eps_r=0.01):
    loss_val = list()
    k = 0
    grad_lst = []
    p = []
    while(True):
        u_pred, A = pred(r, connection_matrix, eds_matrix, get_A=True)
        loss_tmp = loss_calc(u_target, u_pred[u_deriv])
        loss_val. append(loss_tmp)

        grad = grad_i_calc(A, u_target, u_pred, connection_matrix,
                           g, variable_g_inds, u_deriv)[variable_g_inds]
        grad_lst.append(grad)
        if np.linalg.norm(grad) < eps_grad:
            print('eps_g')
            return np.asarray(loss_val), r
        if k >= iterations:
            print('iter')
            return np.asarray(loss_val), r
        if k == 0:
            p.append(-grad_lst[k])
            new_r = onedim_optim(
                u_target, u_deriv, r, variable_g_inds, connection_matrix, eds_matrix, p[k])
        else:
            beta = np.linalg.norm(
                grad_lst[k])**2/np.linalg.norm(grad_lst[k-1])**2
            p.append(grad + beta*p[k-1])
            new_r = onedim_optim(u_target, u_deriv, r.copy(
            ), variable_g_inds, connection_matrix, eds_matrix, p[k])

            if np.linalg.norm(new_r - r) < eps_r:
                u1 = pred(r, connection_matrix, eds_matrix)
                loss1 = loss_calc(u_target, u1[u_deriv])
                u2 = pred(new_r, connection_matrix, eds_matrix)
                loss2 = loss_calc(u_target, u2[u_deriv])
                if np.linalg.norm(loss2 - loss1) < eps_r:
                    print('eps_r')
                    return np.asarray(loss_val), r
        r = new_r.copy()
        k = k+1


def get_Hesse_matrix(r, variable_g_inds):
    hesse = np.zeros((len(variable_g_inds), len(variable_g_inds)))
    i = np.diag_indices_from(hesse)
    hesse[i] = 2/(r[variable_g_inds]**3)
    return hesse


def get_hessian(r, get_hesse_mtx=False):
    hesse = get_Hesse_matrix(r, variable_g_inds)
    for i in range(len(hesse)):
        print(hesse[i, i]*r[i]**2)
    hessian = np.linalg.det(hesse)
    if get_hesse_mtx:
        return hessian, hesse
    return hessian


def Newton(u_target, u_deriv, r, variable_g_inds, connection_matrix, eds_matrix, iterations, eps_grad=0.1, eps_r=0.1):
    loss_val = []
    k = 0
    while True:
        u_pred, A = pred(r, connection_matrix, eds_matrix, get_A=True)
        loss_tmp = loss_calc(u_target, u_pred[u_deriv])
        loss_val.append(loss_tmp)
        grad = grad_i_calc(A, u_target, u_pred, connection_matrix,
                           g, variable_g_inds, u_deriv)[variable_g_inds]
        if np.linalg.norm(grad) < eps_grad:
            return np.asarray(loss_val), r
        if k > iterations:
            return np.asarray(loss_val), r
        hesse = get_Hesse_matrix(r, variable_g_inds)
        try:
            hesse_inv = np.linalg.inv(hesse)
        except Exception:
            return np.asarray(loss_val), r

        dets = []
        for i in range(len(hesse_inv)):
            if (np.linalg.det(hesse_inv[i:, i:]) > 0):
                dets.append(True)
            else:
                dets.append(False)
        if all(dets):
            p = np.matmul(-hesse_inv, grad)
            tmp_r = np.zeros((len(r), ))
            tmp_r[variable_g_inds] = p
            is_step = False
            while(not is_step):
                if all(r + tmp_r > 0):
                    new_r = r + tmp_r
                    if any(new_r > 100000000):
                        return np.asarray(loss_val), r
                    is_step = True
                else:
                    tmp_r = tmp_r/2
        else:
            p = -grad
            new_r = onedim_optim(
                u_target, u_deriv, r, variable_g_inds, connection_matrix, eds_matrix, p)

        if np.linalg.norm(new_r - r) < eps_r:
            u1 = pred(r, connection_matrix, eds_matrix)
            loss1 = loss_calc(u_target, u1[u_deriv])
            u2 = pred(new_r, connection_matrix, eds_matrix)
            loss2 = loss_calc(u_target, u2[u_deriv])
            if np.linalg.norm(loss2 - loss1) < eps_r:
                print('eps_r')
                return np.asarray(loss_val), r

        r = new_r.copy()
        k = k+1


def Marquardt(u_target, u_deriv, r, variable_g_inds, connection_matrix, eds_matrix, iterations, eps_grad=0.1, eps_r=0.1):
    tmp_r = np.zeros((len(r), ))
    loss_val = []
    k = 0
    mu = 1
    while (True):
        u_pred, A = pred(r, connection_matrix, eds_matrix, get_A=True)
        loss_tmp = loss_calc(u_target, u_pred[u_deriv])
        loss_val.append(loss_tmp)
        grad = grad_i_calc(A, u_target, u_pred, connection_matrix,
                           g, variable_g_inds, u_deriv)[variable_g_inds]
        if np.linalg.norm(grad) < eps_grad:
            return np.asarray(loss_val), r
        if k > iterations:
            return np.asarray(loss_val), r
        hesse = get_Hesse_matrix(r, variable_g_inds)
        hesse_E = hesse + mu*np.eye(len(hesse))
        hesse_E_inv = np.linalg.inv(hesse_E)
        p = np.matmul(-hesse_E_inv, grad)
        tmp_r[variable_g_inds] = p
        new_r = r + tmp_r

        u_new_pred, A = pred(new_r, connection_matrix, eds_matrix, get_A=True)
        loss_new_tmp = loss_calc(u_target, u_new_pred[u_deriv])

        to_next_step = False
        while(not to_next_step):
            if(loss_new_tmp <= loss_tmp):
                to_next_step = True
                k = k+1
                mu = mu/2
                r = new_r.copy()
            else:
                mu = 2*mu
                print(new_r)
                hesse_E = hesse + mu*np.eye(len(hesse))
                hesse_E_inv = np.linalg.inv(hesse_E)
                p = np.matmul(-hesse_E_inv, grad)
                tmp_r[variable_g_inds] = p
                new_r = r + tmp_r

                u_new_pred, A = pred(
                    new_r, connection_matrix, eds_matrix, get_A=True)
                loss_new_tmp = loss_calc(u_target, u_new_pred[u_deriv])
