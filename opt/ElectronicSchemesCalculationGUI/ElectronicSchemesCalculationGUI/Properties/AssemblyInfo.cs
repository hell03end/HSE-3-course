﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Common project info
[assembly: AssemblyTitle("ElectronicSchemesCalculationGUI")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("HSE")]
[assembly: AssemblyProduct("ElectronicSchemesCalculationGUI")]
[assembly: AssemblyCopyright("Copyright © hell03end 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// COM objects configuration
[assembly: ComVisible(false)]
[assembly: Guid("9dd69417-5fcf-43fb-985c-08258b068bab")]  // for COM objects

// [main version number].[additional version number].[build number].[redaction]
// use * to apply defaults values
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
