﻿using System;
using System.Windows.Forms;

namespace ElectronicSchemesCalculationGUI
{
    public partial class MainWindow : Form
    {
        public MainWindow() => InitializeComponent();

        private const int netCount = 6;
        private const int UCount = 3;

        private readonly Double[] J = new Double[netCount] { 0, 0, 0, 0, 0, 1 };
        private readonly Double[,] connectionMatrix = new Double[UCount, netCount] {
            { 1.0,  0.0,  0.0, -1.0,  0.0,  1.0 },
            { -1.0,  1.0, -1.0,  0.0,  0.0,  0.0 },
            { 0.0,  0.0,  1.0,  1.0, -1.0,  0.0 }
        };

        private Double[] R = new Double[netCount];
        private Double[] E = new Double[netCount] { 0, 0, 0, 0, 0, 0 };
        private Double[] U = new Double[UCount];

        #region Events

        private void CalculateU(object sender, EventArgs e)
        {
            R[0] = Double.Parse(textBoxR1.Text.Length > 0 ? textBoxR1.Text : "0");
            R[1] = Double.Parse(textBoxR2.Text.Length > 0 ? textBoxR2.Text : "0");
            R[2] = Double.Parse(textBoxR3.Text.Length > 0 ? textBoxR3.Text : "0");
            R[3] = Double.Parse(textBoxR4.Text.Length > 0 ? textBoxR4.Text : "0");
            R[4] = Double.Parse(textBoxR5.Text.Length > 0 ? textBoxR5.Text : "0");
            R[5] = Double.Parse(textBoxR6.Text.Length > 0 ? textBoxR6.Text : "0");
            E[5] = Double.Parse(textBoxEj.Text.Length > 0 ? textBoxEj.Text : "0");

            U = Solve(R, E, connectionMatrix, J);

            DisplayU();
            Console.WriteLine();
        }

        private void buttonCalculateRClick(object sender, EventArgs e)
        {
            Double[] UTarget = (Double[]) U.Clone();
            if (textBoxU1Target.Text != "")
            {
                UTarget[0] = Double.Parse(textBoxU1Target.Text);
            }
            else if (textBoxU2Target.Text != "")
            {
                UTarget[1] = Double.Parse(textBoxU2Target.Text);
            }
            else if (textBoxU3Target.Text != "")
            {
                UTarget[2] = Double.Parse(textBoxU3Target.Text);
            } else
            {
                MessageBox.Show("Enter one target U value!");
                return;
            }

            int maxIter = (int) Double.Parse(textBoxMaxIter.Text.Length > 1 ? textBoxMaxIter.Text : "10000");
            Double gradientStep = Double.Parse(textBoxStep.Text.Length > 1 ? textBoxStep.Text : "0.01");
            Double deltaThreshold = Double.Parse(textBoxDelta.Text.Length > 1 ? textBoxDelta.Text : "0.01");

            int iter = 0;
            Double[] newU = U;
            Double uMAE = MAE(U, UTarget);
            while (uMAE > deltaThreshold && iter < maxIter)
            {
                int idx = (int) iter % netCount;
                R[idx] += gradientStep;
                newU = Solve(R, E, connectionMatrix, J);
                if (newU == null)
                {
                    MessageBox.Show("Can't calculate gradient!");
                    return;
                }

                if (uMAE < MAE(newU, UTarget))
                {
                    // Make step in another direction
                    R[idx] -= 2*gradientStep;
                    newU = Solve(R, E, connectionMatrix, J);
                }

                uMAE = MAE(newU, UTarget);
                iter++;
            }

            U = newU;
            if (iter == maxIter)
            {
                MessageBox.Show($"Iterations limit was reached ({maxIter})!");
            }
            else if (MAE(U, UTarget) <= deltaThreshold)
            {
                MessageBox.Show("Optimal values were found!");
            }

            DisplayU();
            DisplayR();
            Console.WriteLine();
        }

        #endregion

        #region Private

        private void DisplayU()
        {
            if (U == null)
            {
                labelU1.Text = "-";
                labelU2.Text = "-";
                labelU3.Text = "-";
            }
            else
            {
                for (int i = 0; i < U.GetLength(0); ++i)
                {
                    U[i] = Math.Round(U[i], 3);
                }
                labelU1.Text = U[0].ToString();
                labelU2.Text = U[1].ToString();
                labelU3.Text = U[2].ToString();
            }
        }

        private void DisplayR()
        {
            textBoxR1.Text = R[0].ToString();
            textBoxR2.Text = R[1].ToString();
            textBoxR3.Text = R[2].ToString();
            textBoxR4.Text = R[3].ToString();
            textBoxR5.Text = R[4].ToString();
            textBoxR6.Text = R[5].ToString();
        }

        #endregion

        #region Private Static

        /// <summary>
        /// Calculate Mean Absolute Error.
        /// </summary>
        private static Double MAE(Double[] x, Double[] y)
        {
            int size = x.GetLength(0);
            Double res = 0.0;
            for (int i = 0; i < size; ++i)
            {
                res += Math.Abs(x[i] - y[i]);
            }
            return res / size;
        }

        /// <summary>
        /// Calculate Mean Squared Error.
        /// </summary>
        private static Double MSE(Double[] x, Double [] y)
        {
            int size = x.GetLength(0);
            Double res = 0.0;
            for (int i = 0; i < size; ++i) {
                res += Math.Pow(x[i] - y[i], 2);
            }
            return res / size;
        }

        /// <summary>
        /// Solve Equation for electronic scheme
        /// </summary>
        /// <param name="R">Vertor of sheme nodes resistance</param>
        /// <param name="E">Vector of sheme nodes U</param>
        /// <param name="A">Connection Matrix</param>
        /// <param name="J">Vector of sheme nodes I</param>
        /// <returns></returns>
        private static Double[] Solve(Double[] R, Double[] E, Double[,] A, Double[] J)
        {
            Double[] y = CalculateY(R);
            Double[,] Y = MatrixCreate(netCount, netCount);
            for (int i = 0; i < netCount; ++i)
            {
                Y[i, i] = y[i];
            }

            // Right side of equation
            Double[,] AY = MatrixDotMatrix(
                MatrixDotMatrix(A, Y),
                MatrixT(A)
            );

            // Left side of equation
            Double[] IE = MatrixDotVector(Y, E);
            for (int i = 0; i < netCount; ++i)
            {
                IE[i] += J[i];
            }
            Double[] I = MatrixDotVector(A, IE);
            for (int i = 0; i < UCount; ++i)
            {
                Console.WriteLine($"Calculate I{i + 1} = {-I[i]}");
                I[i] = -I[i];
            }

            return SolveWithMatrix(AY, I);
        }

        /// <summary>
        /// Calculate Ys from Rs.
        /// </summary>
        private static Double[] CalculateY(Double[] R)
        {
            int size = R.GetLength(0);
            Double[] Y = new Double[size];
            for (int i = 0; i < size; ++i)
            {
                Y[i] = 1 / R[i];
                Console.WriteLine($"Calculate Y{i + 1} = {Y[i]}");
            }
            return Y;
        }

        /// <summary>
        /// Solve System of Linear Equations with matrix method.
        /// https://ru.wikipedia.org/wiki/Матричный_метод
        /// </summary>
        private static Double[] SolveWithMatrix(Double[,] A, Double[] b)
        {
            bool isSolvable = false;
            try
            {
                isSolvable = MatrixIsSolvable(A);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            if (!isSolvable)
            {
                MessageBox.Show("Can't solve this equation!");
                return null;
            }

            Double[,] complimentMatrix = MatrixComplement(A);
            Double[] res = MatrixDotVector(MatrixT(complimentMatrix), b);
            for (int i = 0; i < res.GetLength(0); ++i)
            {
                res[i] /= MatrixDeterminant(A);
                Console.WriteLine($"Calculate U{i + 1} = {res[i]}");
            }

            return res;
        }

        /// <summary>
        /// Return submatrix of given matrix (without one row and column).
        /// </summary>
        private static Double[,] MatrixGetSubmatrix(Double [,] matrix, int rowExclude, int colExclude)
        {
            int rows = matrix.GetLength(0);
            int cols = matrix.GetLength(1);
            Double[,] submatrix = MatrixCreate(rows - 1, cols - 1);
            for (int i = 0, ti = 0; ti < rows - 1; ++i, ++ti)
            {
                for (int j = 0, tj = 0; tj < cols - 1; ++j, ++tj)
                {
                    if (i == rowExclude)
                    {
                        i++;
                    }
                    if (j == colExclude)
                    {
                        j++;
                    }
                    submatrix[ti, tj] = matrix[i, j];
                }
            }
            return submatrix;
        }

        /// <summary>
        /// Calculate compliment matrix.
        /// </summary>
        private static Double[,] MatrixComplement(Double[,] matrix)
        {
            int rows = matrix.GetLength(0);
            int cols = matrix.GetLength(1);
            Double[,] complementMatrix = MatrixCreate(rows, cols);
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < cols; ++j)
                {
                    Double[,] submatrix = MatrixGetSubmatrix(matrix, i, j);
                    complementMatrix[i, j] = Math.Pow(-1.0, (double)(i + j)) * MatrixDeterminant(submatrix);
                }
            }
            return complementMatrix;
        }

        /// <summary>
        /// Perform matrix T operation.
        /// </summary>
        private static Double[,] MatrixT(Double[,] matrix)
        {
            int rows = matrix.GetLength(0);
            int cols = matrix.GetLength(1);
            Double [,] matrixT = MatrixCreate(cols, rows);
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < cols; ++j)
                {
                    matrixT[j, i] = matrix[i, j];
                }
            }
            return matrixT;
        }

        /// <summary>
        /// Perform matrix * vector multiplication.
        /// </summary>
        private static Double[] MatrixDotVector(Double[,] A, Double[] b)
        {
            int rows = A.GetLength(0);
            int cols = A.GetLength(1);
            Double[] res = new Double[rows];

            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < cols; ++j)
                {
                    res[i] += A[i, j]*b[j];
                }
            }

            return res;
        }

        /// <summary>
        /// Perform matrix * matrix multiplication.
        /// </summary>
        private static Double[,] MatrixDotMatrix(Double[,] A, Double[,] B)
        {
            int rows = A.GetLength(0);
            int cols = B.GetLength(1);
            int hidden = A.GetLength(1);
            Double[,] res = MatrixCreate(rows, cols);

            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < cols; ++j)
                {
                    for (int k = 0; k < hidden; ++k) {
                        res[i, j] += A[i, k]*B[k, j];
                    }
                }
            }

            return res;
        }

        /// <summary>
        /// Check matrix has det != 0
        /// </summary>
        private static bool MatrixIsSolvable(Double[,] matrix)
        {
            //int[] perm;
            //int toggle;
            //Double[,] luMatrix = MatrixDecompose(matrix, out perm, out toggle);

            //Double[,] lower = ExtractLower(luMatrix);
            //Double[,] upper = ExtractUpper(luMatrix);

            if (MatrixDeterminant(matrix) == 0)
                return false;
            return true;
        }

        /// <summary>
        /// allocates/creates a matrix initialized to all 0.0
        /// assume rows and cols > 0
        /// </summary>
        private static Double[,] MatrixCreate(int rows, int cols)
        {
            if (rows < 1)
            {
                throw new ArgumentOutOfRangeException("rows", "Dimension should be > 0");
            }
            if (cols < 1)
            {
                throw new ArgumentOutOfRangeException("cols", "Dimension should be > 0");
            }
            return new Double[rows, cols];
        }

        /// <summary>
        /// Doolittle LUP decomposition with partial pivoting.
        /// Works fine only for square matrix.
        /// </summary>
        /// <returns>
        /// result is L (with 1s on diagonal) and U
        /// perm holds row permutations
        /// toggle is +1 or -1 (even or odd)
        /// </returns>
        private static Double[,] MatrixDecompose(Double[,] matrix, out int[] perm, out int toggle)
        {
            int rows = matrix.GetLength(0);
            int cols = matrix.GetLength(1);

            if (rows != cols)
            {
                throw new Exception("Attempt to MatrixDecompose a non-square mattrix");
            }

            Double[,] result = MatrixDuplicate(matrix);

            perm = new int[rows];
            for (int i = 0; i < rows; ++i) {
                perm[i] = i;
            }

            /*
             * toggle tracks row swaps. +1 -> even, -1 -> odd.
             * used by MatrixDeterminant
             */
            toggle = 1;
            for (int j = 0; j < rows - 1; ++j)
            {
                Double colMax = Math.Abs(result[j, j]); // find largest value in col j
                int pRow = j;
                for (int i = j + 1; i < rows; ++i)
                {
                    if (result[i, j] > colMax)
                    {
                        colMax = result[i, j];
                        pRow = i;
                    }
                }

                if (pRow != j) // if largest value not on pivot, swap rows
                {
                    Double[] rowPtr = new Double[result.GetLength(1)];

                    /*
                     * in order to preserve value of j new variable k for counter is declared
                     * rowPtr[] is a 1D array that contains all the elements on a single row of the matrix
                     * there has to be a loop over the columns to transfer the values
                     * from the 2D array to the 1D rowPtr array.
                     */

                    #region tranfer 2D array to 1D array

                    for (int k = 0; k < result.GetLength(1); k++)
                    {
                        rowPtr[k] = result[pRow, k];
                    }

                    for (int k = 0; k < result.GetLength(1); k++)
                    {
                        result[pRow, k] = result[j, k];
                    }

                    for (int k = 0; k < result.GetLength(1); k++)
                    {
                        result[j, k] = rowPtr[k];
                    }

                    #endregion

                    int tmp = perm[pRow]; // and swap perm info
                    perm[pRow] = perm[j];
                    perm[j] = tmp;

                    toggle = -toggle; // adjust the row-swap toggle
                }

                // if diagonal after swap is zero
                if (Math.Abs(result[j, j]) < 1.0E-20) {
                    throw new Exception("Can't Decompose matrix: diagonal is zero.");
                }

                for (int i = j + 1; i < rows; ++i)
                {
                    result[i, j] /= result[j, j];
                    for (int k = j + 1; k < rows; ++k)
                    {
                        result[i, k] -= result[i, j] * result[j, k];
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Matrix Determinant calculation.
        /// </summary
        private static Double MatrixDeterminant(Double[,] matrix)
        {
            int[] perm;
            int toggle;
            Double[,] lum = MatrixDecompose(matrix, out perm, out toggle);
            if (lum == null)
            {
                throw new Exception("Unable to compute MatrixDeterminant");
            }

            Double result = toggle;
            for (int i = 0; i < lum.GetLength(0); ++i)
            {
                result *= lum[i, i];
            }
            return result;
        }

        /// <summary>
        /// Allocates/creates a duplicate of a matrix.
        /// Assumes matrix is not null.
        /// </summary>
        private static Double[,] MatrixDuplicate(Double[,] matrix)
        {
            Double[,] result = MatrixCreate(matrix.GetLength(0), matrix.GetLength(1));
            for (int i = 0; i < matrix.GetLength(0); ++i)
            {
                for (int j = 0; j < matrix.GetLength(1); ++j)
                {
                    result[i, j] = matrix[i, j];
                }
            }
            return result;
        }

        /// <summary>
        /// Lower part of a Doolittle decomposition (1.0s on diagonal, 0.0s in upper)
        /// </summary>
        private static Double[,] ExtractLower(Double[,] matrix)
        {
            int rows = matrix.GetLength(0);
            int cols = matrix.GetLength(1);
            Double[,] result = MatrixCreate(rows, cols);
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < cols; ++j)
                {
                    if (i == j)
                    {
                        result[i, j] = 1.0f;
                    }
                    else if (i > j)
                    {
                        result[i, j] = matrix[i, j];
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Upper part of a Doolittle decomposition (0.0s in the strictly lower part)
        /// </summary>
        private static Double[,] ExtractUpper(Double[,] matrix)
        {
            int rows = matrix.GetLength(0); int cols = matrix.GetLength(1);
            Double[,] result = MatrixCreate(rows, cols);
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < cols; ++j)
                {
                    if (i <= j)
                    {
                        result[i, j] = matrix[i, j];
                    }
                }
            }
            return result;
        }

        #endregion
    }
}
