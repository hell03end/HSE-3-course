<Query Kind="Program" />

public void Main()
{
	Double[,] a = MatrixCreate(3, 3);
	Double[] b = new Double[3] { 4.0, 9.0, 2.0 };
	a[0, 0] = 1.0; a[0, 1] = 2.0; a[0, 2] = 3.0;
	a[1, 0] = 9.0; a[1, 1] = 2.0; a[1, 2] = 7.0;
	a[2, 0] = 4.0; a[2, 1] = 5.0; a[2, 2] = 8.0;
	Console.WriteLine(MatrixDotMatrix(a, a));
}

public Double[,] MatrixDotMatrix(Double[,] A, Double[,] B)
{
	int rows = A.GetLength(0);
	int cols = B.GetLength(1);
	int hidden = A.GetLength(1);
	Double[,] res = MatrixCreate(rows, cols);

	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
		{
			for (int k = 0; k < hidden; ++k)
			{
				res[i, j] += A[i, k] * B[k, j];
			}
		}
	}

	return res;
}

public Double[] SolveWithMatrix(Double[,] A, Double[] b)
{
	bool isSolvable = false;
	try
	{
		isSolvable = MatrixIsSolvable(A);
	}
	catch (Exception e)
	{
		Console.WriteLine(e.Message);
	}

	if (!isSolvable)
	{
		return null;
	}

	Double[,] complimentMatrix = MatrixComplement(A);
	Double[] res = MatrixDotVector(MatrixT(complimentMatrix), b);
	for (int i = 0; i < res.GetLength(0); ++i)
	{
		res[i] /= MatrixDeterminant(A);
	}

	return res;
}

public Double[,] MatrixGetSubmatrix(Double[,] matrix, int rowExclude, int colExclude)
{
	int rows = matrix.GetLength(0);
	int cols = matrix.GetLength(1);
	Double[,] submatrix = MatrixCreate(rows - 1, cols - 1);
	for (int i = 0, ti = 0; ti < rows - 1; ++i, ++ti)
	{
		for (int j = 0, tj = 0; tj < cols - 1; ++j, ++tj)
		{
			if (i == rowExclude)
			{
				i++;
			}
			if (j == colExclude)
			{
				j++;
			}
			submatrix[ti, tj] = matrix[i, j];
		}
	}
	return submatrix;
}

public Double[,] MatrixComplement(Double[,] matrix)
{
	int rows = matrix.GetLength(0);
	int cols = matrix.GetLength(1);
	Double[,] complementMatrix = MatrixCreate(rows, cols);
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
		{
			Double[,] submatrix = MatrixGetSubmatrix(matrix, i, j);
			complementMatrix[i, j] = Math.Pow(-1.0, (double)(i + j)) * MatrixDeterminant(submatrix);
		}
	}
	return complementMatrix;
}

public Double[,] MatrixT(Double[,] matrix)
{
	int rows = matrix.GetLength(0);
	int cols = matrix.GetLength(1);
	Double[,] matrixT = MatrixCreate(rows, cols);
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
		{
			matrixT[j, i] = matrix[i, j];
		}
	}
	return matrixT;
}

public Double[] MatrixDotVector(Double[,] A, Double[] b)
{
	int rows = A.GetLength(0);
	int cols = A.GetLength(1);
	Double[] res = new Double[rows];

	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
		{
			res[i] += A[i, j] * b[j];
		}
	}

	return res;
}

public bool MatrixIsSolvable(Double[,] matrix)
{
	if (MatrixDeterminant(matrix) == 0)
		return false;
	return true;
}

public Double[,] MatrixCreate(int rows, int cols)
{
	if (rows < 1)
	{
		throw new ArgumentOutOfRangeException("rows", "Dimension should be > 0");
	}
	if (cols < 1)
	{
		throw new ArgumentOutOfRangeException("cols", "Dimension should be > 0");
	}
	return new Double[rows, cols];
}

public Double[,] MatrixDecompose(Double[,] matrix, out int[] perm, out int toggle)
{
	int rows = matrix.GetLength(0);
	int cols = matrix.GetLength(1);

	if (rows != cols)
	{
		throw new Exception("Attempt to MatrixDecompose a non-square mattrix");
	}

	Double[,] result = MatrixDuplicate(matrix);

	perm = new int[rows];
	for (int i = 0; i < rows; ++i)
	{
		perm[i] = i;
	}

	toggle = 1;
	for (int j = 0; j < rows - 1; ++j)
	{
		Double colMax = Math.Abs(result[j, j]); // find largest value in col j
		int pRow = j;
		for (int i = j + 1; i < rows; ++i)
		{
			if (result[i, j] > colMax)
			{
				colMax = result[i, j];
				pRow = i;
			}
		}

		if (pRow != j) // if largest value not on pivot, swap rows
		{
			Double[] rowPtr = new Double[result.GetLength(1)];
			
			#region tranfer 2D array to 1D array

			for (int k = 0; k < result.GetLength(1); k++)
			{
				rowPtr[k] = result[pRow, k];
			}

			for (int k = 0; k < result.GetLength(1); k++)
			{
				result[pRow, k] = result[j, k];
			}

			for (int k = 0; k < result.GetLength(1); k++)
			{
				result[j, k] = rowPtr[k];
			}

			#endregion

			int tmp = perm[pRow]; // and swap perm info
			perm[pRow] = perm[j];
			perm[j] = tmp;

			toggle = -toggle; // adjust the row-swap toggle
		}

		// if diagonal after swap is zero
		if (Math.Abs(result[j, j]) < 1.0E-20)
		{
			throw new Exception("Can't Decompose matrix: diagonal is zero.");
		}

		for (int i = j + 1; i < rows; ++i)
		{
			result[i, j] /= result[j, j];
			for (int k = j + 1; k < rows; ++k)
			{
				result[i, k] -= result[i, j] * result[j, k];
			}
		}
	}

	return result;
}

public Double MatrixDeterminant(Double[,] matrix)
{
	int[] perm;
	int toggle;
	Double[,] lum = MatrixDecompose(matrix, out perm, out toggle);
	if (lum == null)
	{
		throw new Exception("Unable to compute MatrixDeterminant");
	}

	Double result = toggle;
	for (int i = 0; i < lum.GetLength(0); ++i)
	{
		result *= lum[i, i];
	}
	return result;
}

public Double[,] MatrixDuplicate(Double[,] matrix)
{
	Double[,] result = MatrixCreate(matrix.GetLength(0), matrix.GetLength(1));
	for (int i = 0; i < matrix.GetLength(0); ++i)
	{
		for (int j = 0; j < matrix.GetLength(1); ++j)
		{
			result[i, j] = matrix[i, j];
		}
	}
	return result;
}